#!/usr/bin/python

# Copyright 2012 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
import calendar
from datetime import datetime
import cookielib
import urllib, urllib2
import couchdb
import feedparser
import lxml.html
import xml.sax
import sys
import json

# retrieve URL including authentication credentials from config JSON
options = json.load(open('./options.json', 'rt'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]

couch2 = couchdb.Server(options['localcouch'])
dbfeeds = couch2[options['dbfeeds']]

# get the last time for a moodle post in the database
#view = db.view('ids/moodle', descending=True, limit=1)
#if len(view) == 1:
#    for row in view:
#        lasttime = row.key
#else:
#    lasttime = "2000-01-01T00:00:00.000Z"

# return a tuple (text, truncated) of abridged text and truncated flag
def abridge(text):
    truncated = False
    # strip out HTML comments (and MSOffice conditionals)
    abridged = re.sub(r'<!--.*?-->', '', text)
    # strip out HTML markup before abridging,
    #   so we don't stop midtag
    abridged = re.sub(r'<[^>]*>', ' ', abridged)
    abridged = re.sub(r'\s*by [^.]+\.\n?', '', abridged)
    abridged = abridged[:500].strip()
    abridged = abridged.replace('&nbsp;', ' ')
    abridged = abridged.replace('&#8211;', "--")
    abridged = abridged.replace('&#8216;', "'")
    abridged = abridged.replace('&#8217;', "'")
    abridged = abridged.replace('&#8220;', '"')
    abridged = abridged.replace('&#8221;', '"')
    abridged = abridged.replace('&#8230;', "...")
    abridged = abridged.replace('&#38;', "&")
    abridged = abridged.replace('\n', ' ')
    # get rid of multiple spaces (which the above may have introduced)
    abridged = re.sub(r'  +', ' ', abridged)
    i = len(abridged)
    if i > 200:
        i = 200
        while abridged[i] != ' ' and i > 0:
            i -= 1
        abridged = abridged[:i] + '...'
        truncated = True
    return (abridged, truncated)

def process_feed(feed):
    new_docs = []
    #print "==", feed.doc['url']
    feed_last = feed.doc['last_updated']
    rss = feedparser.parse(feed.doc['url'])

    #if 'edem630' in feed.doc['tags']:
    #    print "!!edem630 blog, re-check the last week"
    #    feed_last = '2013-07-21T00:00:00.000Z'

    # if there is no timestamp, probably no entries
    if not rss.feed.has_key('updated_parsed') or rss.feed['updated_parsed'] is None:
        return None

    feed_timestamp = time.strftime('%Y-%m-%dT%H:%M:%S.000Z',
            rss.feed['updated_parsed'])

    # check last_updated of feed table
    if feed_timestamp <= feed_last:
        return None     # no need to even bother checking items

    # check last timestamp in the mentions database for this feed
    items = rss['items']
    items.reverse()
    for item in items:
        # FIXME perhaps should query to see if article exists
        #  to avoid "updates" that change pub time
        we_timestamp = time.strftime('%Y-%m-%dT%H:%M:%S.000Z',
                item['updated_parsed'])
        if we_timestamp > feed_last:
            truncated = False
            text = ''
            if len(item['title'].strip()) > 0:
                text = item['title'].strip() + ': '
            text = text + item['summary']
            (abridged, truncated) = abridge(text)
            mention = {
                    'from_user': feed.doc['from_user'],
                    'from_user_name': feed.doc['from_user_name'],
                    'created_at': item['updated'],
                    'profile_url': feed.doc['profile_url'],
                    'profile_image_url': feed.doc['profile_image_url'],
                    'title': item['title'].strip(),
                    'text': abridged,
                    'truncated': truncated,
                    'id': calendar.timegm(item['updated_parsed']),
                    'we_source': 'feed',
                    'we_feed': rss.feed['title'],
                    'we_feed_url': feed.doc['url'],
                    'we_timestamp': we_timestamp,
                    'we_link': item['link']
                    }

            # if there is an id, use it instead of our made up one
            if item.has_key('id'):
                mention['id'] = item['id']

            # if there is width and/or height, copy them
            if feed.doc.has_key('profile_image_width'):
                mention['profile_image_width'] = \
                        feed.doc['profile_image_width']
            if feed.doc.has_key('profile_image_height'):
                mention['profile_image_height'] = \
                        feed.doc['profile_image_height']

            # if there is a gravatar hash, copy it
            if feed.doc.has_key('gravatar'):
                mention['gravatar'] = feed.doc['gravatar']

            # if tags is empty, we take everything and apply we_tags
            if len(feed.doc['tags']) == 0:
                mention['we_tags'] = feed.doc['we_tags']
                new_docs.append(mention)
            else:
                mention['we_tags'] = []
                # only save things tagged with tags
                # or that mention the tag in the title
                keep = False
                for tg in feed.doc['tags']:
                    if item.has_key('tags'):
                        for tag in item['tags']:
                            #print "==tag: %s tg: %s" % (tag, tg)
                            if tag.has_key('term') and tag['term'].lower().find(tg) > -1 and tg not in mention['we_tags']:
                                mention['we_tags'].append(tg)
                                continue
                    if text.lower().find(tg) > -1 and tg not in mention['we_tags']:
                        #print "==tg: %s in text search" % tg
                        mention['we_tags'].append(tg)

                # keep it if there was one or more interesting tags
                if len(mention['we_tags']) > 0:
                    # see if we already have this one
                    #   only update if tags have changed
                    exists = db.view('ids/feed')
                    ex = exists[mention['we_link']]
                    if len(ex) > 0:
                        # ideally there should be at most one...
                        for exi in ex:
                            if set(mention['we_tags']) <> set(exi['value']):
                                #print "++ need to update tags, old>new", exi['value'], mention['we_tags']
                                odoc = db[exi['id']]
                                odoc['we_tags'] = mention['we_tags']
                                db[exi['id']] = odoc
                                #print "++updated tags in post"
                            else:
                                #print "--old post already has all the tags"
                                pass
                    else:
                        #print "??don't seem to have this post", mention['we_tags']
                        new_docs.append(mention)
                else:
                    #print "!!!Skipping post with no interesting tags."
                    #print "!!!", feed.doc['url']
                    #print "!!!", item
                    pass

    #import pdb; pdb.set_trace()
    if len(new_docs) > 0:
        #print "**** updating %d new docs" % len(new_docs)
        result = db.update(new_docs)
        #print "    ", result
    return time.strftime('%Y-%m-%dT%H:%M:%S.000Z',
            rss.feed['updated_parsed'])

for feed in dbfeeds.view('feed/activerss', include_docs=True):
    # process feeds based on the 'freq' attribute
    # FIXME for now, only on/off, but could be polling frequency
    if feed.doc['freq'] > 0:
        #print feed.id, feed.value, feed.doc
        try:
            last_updated = process_feed(feed)
        except xml.sax._exceptions.SAXException:
            last_updated = None
        if last_updated:
            doc = feed.doc
            doc['last_updated'] = last_updated
            doc['last_successful'] =  time.strftime(
                        '%Y-%m-%dT%H:%M:%S.000Z')
            dbfeeds[feed.id] = doc
