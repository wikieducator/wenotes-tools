#!/usr/bin/python

# Copyright 2018 Open Education Resource Foundation
# developed by Dave Lane dave@oerfoundation.org, with help from
# code written by Jim Tittsler
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
import calendar
from datetime import datetime
import cookielib
import urllib, urllib2
import couchdb
import feedparser
import lxml.html
import xml.sax
import json
# for debugging
import logging
import pprint
# to fix various unicode issues
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# retrieve URL including authentication credentials from config JSON
options = json.load(open('./options.json', 'rt'))

# version info
scanner = 'WEnotes Blog Feed Scanner'
scanner_version = '0.2'

# logging configuration
LogLevel = logging.DEBUG # or logging.INFO or logging.WARN, etc.
#LogLevel = logging.INFO # or logging.INFO or logging.WARN, etc.
LogFilename = options['logdir'] + '/blog_feeds.log'
LogFormat = '%(asctime)s - %(levelname)s: %(message)s'
print 'logfile %s, level %s' % (LogFilename, LogLevel)
logging.basicConfig(format=LogFormat,level=LogLevel,filename=LogFilename)

# configure the Couch db for mentions
couch = couchdb.Server(options['url'])
dbmentions = couch[options['db']]

# configure the Couch db for finding the feed details
couch2 = couchdb.Server(options['localcouch'])
dbfeeds = couch2[options['dbfeeds']]

# Set up the prettyprinter object for debugging
pp = pprint.PrettyPrinter(indent=4)

# return a tuple (text, truncated) of abridged text and truncated flag
def abridge(text):
    truncated = False
    # strip out HTML comments (and MSOffice conditionals)
    abridged = re.sub(r'<!--.*?-->', '', text)
    # strip out HTML markup before abridging,
    #   so we don't stop midtag
    abridged = re.sub(r'<[^>]*>', ' ', abridged)
    abridged = re.sub(r'\s*by [^.]+\.\n?', '', abridged)
    abridged = abridged[:500].strip()
    abridged = abridged.replace('&nbsp;', ' ')
    abridged = abridged.replace('&#8211;', "--")
    abridged = abridged.replace('&#8216;', "'")
    abridged = abridged.replace('&#8217;', "'")
    abridged = abridged.replace('&#8220;', '"')
    abridged = abridged.replace('&#8221;', '"')
    abridged = abridged.replace('&#8230;', "...")
    abridged = abridged.replace('&#38;', "&")
    abridged = abridged.replace('\n', ' ')
    # get rid of multiple spaces (which the above may have introduced)
    abridged = re.sub(r'  +', ' ', abridged)
    i = len(abridged)
    if i > 200:
        i = 200
        while abridged[i] != ' ' and i > 0:
            i -= 1
        abridged = abridged[:i] + '...'
        truncated = True
    return (abridged, truncated)

def process_feed(feed):
    new_docs = []
    doc = feed.doc
    logging.debug("== %s", doc['feed_url'])
    try:
        feed_last = doc['last_checked']
    except KeyError:
        feed_last = None
    type = doc['feed_type']
    url = doc['feed_url']
    if type == 'application/rss+xml':
        logging.debug('processing RSS feed %s', url)
        content = feedparser.parse(url)
    elif type == 'application/atom+xml':
        logging.debug('processing Atom feed %s', url)
        content = feedparser.parse(url)
    else:
        logging.debug('unsupported feed type %s ', type)

    # print the feed...
    #logging.debug("------ the feed ------ %s", pp.pformat(feed));

    # if there is no timestamp, probably no entries
    #if not content.feed.has_key('updated_parsed') or content.feed['updated_parsed'] is None:
    try:
        if not content.feed.has_key('updated_parsed'):
            return None
    except UnboundLocalError:
        logging.debug('content object not defined')
        return None

    feed_timestamp = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', content.feed['updated_parsed'])

    # check last_updated of feed table
    if feed_timestamp <= feed_last:
        return None     # no need to even bother checking items

    # check last timestamp in the mentions database for this feed
    items = content['items']
    items.reverse()
    for item in items:
        # FIXME perhaps should query to see if article exists
        #  to avoid "updates" that change pub time
        we_timestamp = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', item['updated_parsed'])
        if we_timestamp > feed_last:
            truncated = False
            text = ''
            if len(item['title'].strip()) > 0:
                text = item['title'].strip() + ': '
            text = text + item['summary']
            (abridged, truncated) = abridge(text)
            mention = {
                    'from_user': doc['username'],
                    'from_user_name': doc['display_name'],
                    'created_at': item['updated'],
                    'profile_url': item['link'],
                    #'profile_image_url': doc['profile_image_url'],
                    'title': item['title'].strip(),
                    'text': abridged,
                    'truncated': truncated,
                    'id': calendar.timegm(item['updated_parsed']),
                    'we_source': 'feed',
                    'we_identifier': 'blog_feed',
                    'we_scanner': scanner,
                    'we_scanner_version': scanner_version,
                    'we_feed': content.feed['title'],
                    'we_feed_url': doc['feed_url'],
                    'we_timestamp': we_timestamp,
                    'we_link': item['link']
                    }

            # if there is an id, use it instead of our made up one
            if item.has_key('id'):
                mention['id'] = item['id']

            # if there is width and/or height, copy them
            if doc.has_key('profile_image_width'):
                mention['profile_image_width'] = doc['profile_image_width']
            if doc.has_key('profile_image_height'):
                mention['profile_image_height'] = doc['profile_image_height']

            # if there is a gravatar hash, copy it
            if doc.has_key('gravatar'):
                mention['gravatar'] = doc['gravatar']

            # if tags is empty, we take everything and apply we_tags
            if len(doc['tags']) == 0:
                mention['we_tags'] = doc['tags']
                new_docs.append(mention)
            else:
                mention['we_tags'] = []
                # only save things tagged with tags
                # or that mention the tag in the title
                keep = False
                for we_tag in doc['tags']:
                    if item.has_key('tags'):
                        for tag in item['tags']:
                            logging.debug("==tag: %s we_tag: %s", tag, we_tag)
                            if tag.has_key('term') and tag['term'].lower().find(we_tag) > -1 and we_tag not in mention['we_tags']:
                                mention['we_tags'].append(we_tag)
                                continue
                    if text.lower().find(we_tag) > -1 and we_tag not in mention['we_tags']:
                        logging.debug("==we_tag: %s in text search", we_tag)
                        mention['we_tags'].append(we_tag)

                # keep it if there was one or more interesting tags
                if len(mention['we_tags']) > 0:
                    # see if we already have this one
                    #   only update if tags have changed
                    existing = dbmentions.view('ids/feed')
                    exists = existing[mention['we_link']]
                    if len(exists) > 0:
                        # ideally there should be at most one...
                        for exi in exists:
                            if set(mention['we_tags']) <> set(exi['value']):
                                logging.debug("++ need to update tags, old>new %s %s", exi['value'], mention['we_tags'])
                                odoc = dbmentions[exi['id']]
                                odoc['we_tags'] = mention['we_tags']
                                dbmentions[exi['id']] = odoc
                                logging.debug("++updated tags in post")
                            else:
                                logging.debug("--old post already has all the tags")
                                pass
                    else:
                        logging.debug("??don't seem to have this post: %s", mention['we_tags'])
                        new_docs.append(mention)
                else:
                    logging.debug("!!!Skipping post with no interesting tags: %s", doc['feed_url'])
                    #print "!!!", item
                    pass

    #import pdb; pdb.set_trace()
    if len(new_docs) > 0:
        logging.info("**** updating %d new docs", len(new_docs))
        result = dbmentions.update(new_docs)
        logging.debug("    %s", result)
    return time.strftime('%Y-%m-%dT%H:%M:%S.000Z', content.feed['updated_parsed'])

for feed in dbfeeds.view('_design/ids/_view/by_wp_id', include_docs=True):
    try:
        logging.debug('checking feed %s for references to %s', feed.doc['feed_url'], feed.doc['tags'])
        try:
            last_checked = process_feed(feed)
        except xml.sax._exceptions.SAXException:
            last_checked = None
        if last_checked:
            doc = feed.doc
            doc['last_updated'] = last_checked
            doc['last_successful'] =  time.strftime(
                        '%Y-%m-%dT%H:%M:%S.000Z')
            dbfeeds[feed.id] = doc
    except IndexError:
        logging.debug('issue accessing relevant data on doc id %s', feed.doc['_id'])
logging.info("run finished\n")
