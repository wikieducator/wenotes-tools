WEnotes
====

WEnotes consists of several parts:

* harvesting tools (this repository, WEnotes-tools) for:
    * specific blogs - supports WordPress blogs and Blogger.com (db table of RSS feeds)
    * [Hypothesis](https://hypothes.is) web annotations (RSS feed)
    * [Mastodon](https://github.com/tootsuite/mastodon) federated micro-blogging (JSON feed)
    * [Medium Blogs](https://medium.com) a blogging platform with tag & user-based feeds
    * Moodle instances (special RSS feed)
    * [GroupServer](http://groupserver.org/) mailing lists (special RSS feed)
    * [Discourse](https://www.discourse.org/) forums
    * IRC
    * [Rocket.Chat](https://rocket.chat/) integration
    * [Semantic Scuttle](http://semanticscuttle.sourceforge.net/)

* deprecated and/or proprietary service for which we have harvesters we're no longer using.
 May be invalid due to changes in terms, or discontinuation, of service
    * ASKBOT (special RSS feed)
    * g+
    * identica (defunct open source micro-blogging service)
    * Twitter


* a Mediawiki extension (WEnotes repository)
    * verifies user is logged in
    * saves local posts

* Mediawiki Widget for collecting local microblog postings (wiki and WEnotes repository)

* Mediawiki Widget for displaying an integrated display of all notes (wiki and WEnotes repository)

* server (WEnotes-server repository)
    * [Faye](http://faye.jcoglan.com/) pub/sub server
    * simple proxy to CouchDB database (to avoid problems with networks that limit ports)
    * incoming webhook for Rocket.Chat integration
