#!/usr/bin/python

# Copyright 2012 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime
import cookielib
import urllib, urllib2
import couchdb
import sys
import json

def formatDent(dent, tag):
    timestamp = dent['created_at']
    dt = datetime.strptime(dent['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
    dent['we_timestamp'] = dt.isoformat() + '.000Z'
    print dent['we_timestamp']
    dent['we_source'] = 'identica'
    dent['we_tags'] = [tag]
    return dent

def getDents(newDents, tag, sinceID):
    url = 'http://identi.ca/api/statusnet/tags/timeline/%s.json?since_id=%d' % (tag, sinceID)
    response = urllib2.urlopen(url)
    dents = json.loads(response.read())
    for dent in dents:
        if dent['id'] > sinceID:
            id = dent['id']
            if newDents.has_key(id):
                print "appending tag", tag
                newDents[id]['we_tags'].append(tag)
            else:
                print "new interesting dent", id, tag
                newDents[id] = formatDent(dent, tag)

    return

# retrieve URL including authentication credentials from config JSON
options = json.load(open('./options.json', 'rt'))

couch = couchdb.Server(options['url'])
db = couch[options['db']]

# get the last ID for each of the tags
stats = db.view('ids/identica_stats', group=True)
last = {}
for row in stats:
    last[row.key] = row.value['max']

# build a dictionary, indexed by ID so we can quickly merge we_tags
newDents = {}

for tag in options['tags']:
    try:
        getDents(newDents, tag, last.get(tag, 0))
    except urllib2.HTTPError:
        print "identica: unable to fetch tag %s" % tag
        pass

# convert to a list
newDents = newDents.values()
if len(newDents):
    print len(newDents), newDents
    for doc in db.update(newDents):
        print repr(doc)
