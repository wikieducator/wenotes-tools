/*
 * watch the CouchDB mentions database for changes
 * and publish them on the correct Faye channel(s)
 *
 * Copyright 2012-2016 Open Education Resource Foundation
 * @license MIT
 */
"use strict";

var util    = require('util'),
    cradle  = require('cradle'),
    faye    = require('faye'),
    Log     = require('log'),
    fs      = require('fs'),
    options = require('./options.json');

var log = new Log(options.log.couchwatch.level,
              fs.createWriteStream(options.log.couchwatch.file, {
                flags: 'a'}));

var ClientAuth = {
  outgoing: function(message, callback) {
    message.ext = message.ext || {};
    message.ext.password = options.fayepass;
    callback(message);
  }
};

var client = new faye.Client(options.fayeurl);
client.addExtension(ClientAuth);

var db = new(cradle.Connection)(options.couch.url, options.couch.port,
      {auth: {username: options.couch.user, password: options.couch.pass}}).database(options.db),
    avatars = new(cradle.Connection)(options.couch.url, options.couch.port,
      {auth: {username: options.couch.user, password: options.couch.pass}}).database(options.avatars);

var feed = db.changes({
  since: 'now',
  include_docs: true
});

function publish(change) {
  var i, l;
  try {
    if (change.doc.hasOwnProperty('we_tags')) {
      l = change.doc.we_tags.length;
      for (i=0; i<l; i++) {
        log.debug('= publish on ' + change.doc.we_tags[i]);
        client.publish('/WEnotes/' + change.doc.we_tags[i], change.doc);
      }
    } else {
      log.error('mention has no tags', util.inspect(change.doc));
    }
  } catch (err) {
    log.error('publish error', err);
  }
}

feed.on('change', function (change) {
  log.info(util.inspect(change));
  // if it is a WikiEducator WEnote without a profileIMG
  // try to fulfill it here
  if (change.doc.we_source === 'wikieducator'
      && change.doc.profile_image_url === '') {
    avatars.get(change.doc.from_user, function (err, doc) {
      if (!err) {
        log.debug('=== fill in avatar', util.inspect(doc));
        change.doc.profile_image_url = doc.url;
        // changing the profile_img_url will cause another 'change'
        // which is the one that will be published
        db.save(change.doc._id, change.doc._rev, change.doc,
          function (err, res) {
            log.debug('+++ avatar updated', err, util.inspect(res));
        });
      } else {
        publish(change);
      }
    });
  } else {
    publish(change);
  }
});

feed.on('error', function(er) {
  log.error('Serious error trying to follow CouchDB changes');
  throw er;
});
