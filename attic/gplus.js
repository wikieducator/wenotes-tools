var async = require('async'),
    cradle = require('cradle'),
    options = require('./options.json');

var URL = 'https://www.googleapis.com/plus/v1/activities?';
var urls = [];
var query = {key: options.googleapikey,
             maxResults: 20};
var tags = options.tags;

tags = ['wenotes'];

var db = new(cradle.Connection)(options.url).database('mentions'); 

/**
 for each tag
   plusSearch(tag)
     .then(func (v) {
       v.foreach {
         lookup(vi)
           .then(func (vi) {
             if (vi doesn't exist) {
               mentions.push(vi)
             } else {
               if tag not in vi.we_tags
                 vi.we_tags.push(tag);  updateRequired
               if plusones !== vi.plusones
                 vi.plusones = plusones; updateRequired
               if shares !== vi.shares
                 vi.shares = shares; updateRequired
               if comments !== vi.comments
                 processURL(commentURL);
                 vi.comments = comments; updateRequired
               if updateRequired
                 updateDB vi
              }
    if mentions.length
      bulkInsertDB mentions
 **/1G1
             
// return a promise for the last ID in the db
function lastID(tag) {
  var last = 0,
      deferred = Q.defer();

  console.log('query last id for ' + tag);
  db.view('ids/google',
    { startkey: [tag, "2099-12-31T23:59:59.999Z"],
      endkey: [tag, "2011-01-01T00:00:00.000Z"],
      limit: 1,
      descending: true },
    function (err, res) {
      if (err) {
        deferred.reject(new Error(err));
      } else {
        if (res.length === 1 && res[0].id) {
          last = res[0].value;
        }
        deferred.resolve(last);
      }
    });

  return deferred.promise;
}

// return a promise for the g+ items
function googlePlus(url) {
  return HTTP.read(url);
}

var pc = 0,
    p = {};

function lookupIDs(v) {
  var ids = [];
  p = JSON.parse(v.toString());
  if (p && p.hasOwnProperty('items')) {
    pc = p.items.length;
    console.log('*** items: ', pc);

    for (i = 0; i < pc; i++) {
      item = p.items[i];
      ids.push(item.id);
    }

    db.view('ids/google', {key:
      if 
          console.log('-----', last, item.id);
          console.log(item);
          item.we_source = 'g+';
          item.we_tag = tag;
          item.we_timestamp = item.published;
          mentions.unshift(item);
        }
      
    }
  }
}

function processTag() {
  var i,
      lastid = '0',
      mentions = [];

  function qs(o) {
    var qar = [];
    for (k in o) {
      qar.push(k + '=' + encodeURIComponent(o[k]));
    }
    console.log('querystring', qar.join('&'));
    return qar.join('&');
  }

  if (urls.length === 0) return;
  var url = urls.pop();
  console.log('==== ' + url);

  HTTP.read(url)
    .then(lookupIDs(v))
    .then(function (lookups) {
      console.log(lookups);
    }, function (error) {
      console.log(error);
      throw error;
    }).fin(function () {
      process.nextTick(processURLs);
    });

  Q.all([lastID(tag), googlePlus(url)])
      .then(function(v) {
        var last = v[0];
        var p = JSON.parse(v[1].toString());

        if (p && p.items) {
          console.log('*** ', tag, ':', p.items.length, 'items');
        }
        items = p.items.length;
        for (i = 0; i < items; i++) {
          item = p.items[i];
          console.log('-----', last, item.id);
          console.log(item);
          item.we_source = 'g+';
          item.we_tag = tag;
          item.we_timestamp = item.published;
          mentions.unshift(item);
        }
        console.log("mentions", mentions);
        if (mentions.length) {
          db.save(mentions, function (err, res) {
            if (err) {
              console.log('error saving mentions for ' + tag);
              throw err;
            }
            console.log('mentions saved for ' + tag);
          });
        }
      }, function(error) {
        console.log(error)
      }).fin(function() {
        process.nextTick(processURLs);
      });
}

var urlQ = async.queue(function (task, callback) {
  console.log('task:', task);
  callback();
}

for (i = 0; i < tags.length; i++) {
  query.query = '#' + tag;
  urls.push(URL + qs(query);
}

processURLs();
