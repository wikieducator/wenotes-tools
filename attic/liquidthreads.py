#!/usr/bin/python

# Copyright 2012 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime
import cookielib
import urllib, urllib2
import couchdb
import feedparser
import lxml.html
import sys
import json

# retrieve URL including authentication credentials from config JSON
options = json.load(open('options.json', 'rt'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]

# serial number of messages retrieved this poll to uniquify ID
serial = 0

# get the last time for a moodle post in the database
view = db.view('ids/moodle', descending=True, limit=1)
if len(view) == 1:
    for row in view:
        lasttime = row.key
else:
    lasttime = "2000-01-01T00:00:00.000Z"

feeds = options['feedsMoodle']

cj = cookielib.CookieJar()
moodle = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
data = urllib.urlencode({'username': options['moodleuser'],
                        'password': options['moodlepass']})

li = moodle.open('http://moodle.wikieducator.org/login/index.php', data)

feedno = 0
for feed in feeds:
    rss = feedparser.parse(feed)
    feedtitle = rss['channel']['title']

    items = rss['items']
    items.reverse()

    for item in items:
        if item['title'] == 'RSS Error' and item['description'] == 'Error reading RSS data':
            break
        truncated = False
        dt = datetime.strptime(item['date'], '%a, %d %b %Y %H:%M:%S GMT')
        we_timestamp = dt.strftime('%Y-%m-%dT%H:%M:%S.000Z')
        if we_timestamp <= lasttime:
            continue
        seconds = time.mktime(dt.timetuple())
        # strip out HTML markup before abridging, so we don't stop midtag
        abridged = re.sub(r'<[^>]*>', '', item['summary'])
        abridged = re.sub(r'\s*by [^.]+\.\n?', '', abridged)
        abridged = abridged.replace('&nbsp;', ' ')
        abridged = abridged.replace('\n', ' ')
        abridged = abridged.strip()
        i = len(abridged)
        if i > 137:
            i = 137
            while abridged[i] != ' ' and i > 0:
                i -= 1
            abridged = abridged[:i] + '...'
            truncated = True

        # fetch the original article, try to find author/img
        f = moodle.open(item['link'])
        html = lxml.html.parse(f).getroot()
        authordiv = html.find_class('author')[0]
        author = authordiv.findtext('a')
        profile_url = authordiv.find('a').attrib['href']
        pics = html.find_class('userpicture')
        attrs = pics[0].attrib
        imgurl = attrs['src']
        mention = {
                'from_user': author,
                'from_user_name': author,
                'created_at': item['date'],
                'profile_image_url': imgurl,
                'text': abridged,
                'truncated': truncated,
                'id': '%d%02d%03d' % (seconds, feedno, serial),
                'profile_url': profile_url,
                'we_source': 'moodle',
                'we_feed': feedtitle,
                'we_tags': ['oeru'],
                'we_timestamp': we_timestamp,
                'we_link': item['link']
                }
        db.save(mention)
        serial += 1
    feedno += 1

