###
rss - harvest RSS (and Atom) feeds for WEnotes
    - with some special case handling for Moodle and AskBot

Copyright 2012 OER Foundation
@license MIT
###

async   = require 'async'
request = require 'request'
cradle  = require 'cradle'
parser  = require 'xml2json'
options = require './options.json'

feeds = new(cradle.Connection)('http://t.oerfoundation.org', 5984, {
    cache: true
    raw: false
  }
).database 'feeds'

mentions = new(cradle.Connection)('http://t.oerfoundation.org', 5984, {
    cache: false
    raw: false
  }
).database 'test'

###
request.post(
  url: 'http://moodle.wikieducator.org/login/index.php'
  form:
    username: options.moodleuser
    password: options.moodlepass
  (error, response, body) ->
    console.log('error:', error) if error
)
###

itemcount = 0

pollFeed = (feed, callback) ->
  processText = (t, type) ->
    truncated = false
    # unescape
    t = t.replace /&nbsp;/g, ' '
    t = t.replace /&quote;/g, '"'
    t = t.replace /&gt;/g, '>'
    t = t.replace /&lt;/g, '<'
    t = t.replace /&amp;/g, '&'
    # remove inline attribution in ask feeds
    t = t.replace /(Comment|Answer) by (.*?) for'/, '$1 for' if type is 'ask'
    # strip out any HTML markup
    # FIXME work around bug where newlines disappear in RSS->JSON
    t = t.replace /<[^>]*>/g, ' '
    # shrink multiple whitespace
    t = t.replace /\s\s+/g, ' '
    # if it is too long, abridge
    if (t.length > 137)
      li = t.lastIndexOf(' ', 137)
      t = if (li <= 0) then t.substring(0, 137) else t.substring(0, li)
      t += '...'
      truncated = true
    return [t, truncated]

  processItem = (item, feed) ->
    console.log "----"
    console.log item
    dt = item.pubDate || item.date || item.updated
    console.log "----#{item.title}----#{dt}----#{feed.doc.type}"
    we_timestamp = new Date(dt).toISOString()
    switch feed.doc.type || 'unknown'
      when 'ask'
        text = "#{item.title} #{item.summary}"
      when 'moodle'
        text = item.description
      else
        text = item.description
    [abridged, truncated] = processText text, feed.doc.type
    console.log abridged
    id = Math.floor(new Date() / 1000) + ('0000' + itemcount).substr(-5)
    author = ''
    feedtitle = ''
    mention =
      from_user: author
      from_user_name: author
      created_at: item.date
      profile_url: ''
      profile_image_url: ''
      text: abridged
      truncated: truncated
      id: id
      we_source: 'rss'
      we_feed: feedtitle
      we_tag: 'ocl4ed'
      we_timestamp: we_timestamp
      we_link: item.link
    console.log mention
    itemcount += 1

  processFeed = (error, response, body) ->
    j = parser.toJson body, object: true
    console.log j
    # handle RSS and Atom
    items = j.rss?.channel?.item || j.feed?.entry
    lastUpdate = new Date(j.rss?.channel?.lastBuildDate || j.feed?.updated || '1970-01-02')
    console.log "***#{body.length}***#{items.length}***#{lastUpdate.toISOString()}***"
    console.log "   #{lastFeedDate}"
    if lastFeedDate < lastUpdate
      processItem item, feed for item in items
    callback null

  console.log "=================#{feed.doc.url}"
  lastFeedDate = new Date(feed.doc.lastDate || '1970-01-01')
  console.log lastFeedDate
  request feed.doc.url, processFeed

feeds.all 
  include_docs: true
  (err, b) ->
    console.log('allBySeq err:', err) if err
    async.forEachSeries b, pollFeed, (err) ->
      console.log 'pollfeed async loop error' if err

