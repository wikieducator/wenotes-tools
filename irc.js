/*
 * irc -> RocketChat
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
/* jshint node:true, esversion:6 */
"use strict";

var util = require('util'),
    IRC = require('irc'),
    md5 = require('md5'),
    request = require('request'),
    options = require('./options.json');

var channel = options.IRC.ircchannel,
  irc = new IRC.Client('chat.freenode.net', options.IRC.botname, {
  channels: [channel],
});

irc.addListener('message', function(from, to, message) {
  //strip mIRC color codes
  message = message.replace(/\x03(\d\d?(,\d\d?)?)?/g, '');

  //console.log(from + '->' + to + ': ' + message);

  if (to === channel) {
    let hash = md5(from.toLowerCase() + '@irc.freenode.net'),
        notification = {
      channel: options.IRC.RocketChat.channel,
      username: `IRC:${from}`,
      icon_url: `https://www.gravatar.com/avatar/${hash}?s=48&d=identicon`,
      text: message
    };
    request.post({
      uri: options.IRC.RocketChat.webhook,
      json: notification
    }, (error, response, body) => {
      if (error) {
        console.log(`Rocket.Chat POST error: ${error} response: ${util.inspect(response)} body: ${util.inspect(body)}`);
      }
    });
  }
});

irc.addListener('pm', function (from, message) {
  //console.log(from + '->bot: ' + message);
});

irc.addListener('error', function(message) {
  // ignore "already registered" errors
  if (message && message.rawCommand !== '462') {
    console.log('IRC ERROR: ', message);
  }
});

irc.addListener('notice', function(nick, to, text, message) {
  var l = 'IRC Notice: ',
      benign = [
        '*** Looking up your hostname...',
        '*** Found your hostname',
        "*** Couldn't look up your hostname",
        '*** Checking Ident',
        '*** No Ident response'
      ];
  if (nick) {
    l += nick + '->' + to + ': ';
  }
  // strip IRC formatting
  text = text.replace(/\x02|\x03|\x1d|\x1f|\x16|\x0f/g, '');
  text = text.trim();
  // show non-benign notices
  if (benign.indexOf(text) === -1) {
    console.log(`${l}${text}`);
  }
  if (nick === 'NickServ' && text.indexOf('This nickname is registered') >= 0) {
    console.log('Identify to NickServ');
    irc.say('NickServ', 'identify ' + options.IRC.botpass);
  }
});
