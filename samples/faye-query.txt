https://faye.dev.oerfoundation.org/faye/?message=[{"channel":"/meta/handshake","version":"1.0","supportedConnectionTypes":["callback-polling"],"id":"1"}]&jsonp=__jsonp1__

Should result in

/**/__jsonp1__([{"id":"1","channel":"/meta/handshake","successful":true,"version":"1.0","supportedConnectionTypes":["long-polling","cross-origin-long-polling","callback-polling","websocket","eventsource","in-process"],"clientId":"7t317bqsf8ta5pfvqsvh4iss8ll0zmb","advice":{"reconnect":"retry","interval":0,"timeout":45000}}]);

or (formatted)

/**/__jsonp1__([{
  "id":"1",
  "channel":"/meta/handshake",
  "successful":true,
  "version":"1.0",
  "supportedConnectionTypes":[
    "long-polling",
    "cross-origin-long-polling",
    "callback-polling",
    "websocket",
    "eventsource",
    "in-process"
  ],
  "clientId":"7t317bqsf8ta5pfvqsvh4iss8ll0zmb",
  "advice":{
    "reconnect":"retry",
    "interval":0,
    "timeout":45000
  }
}]);
