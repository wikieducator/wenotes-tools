#!/usr/bin/python

""" Harvest messages from forums category URLs specified in options file."""

# Copyright 2016 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
import couchdb
import json
import urllib
import argparse
import requests
# for debugging
import logging
import pprint
# to deal with nasty characters included in Hypothesis quotes
import sys
reload(sys)
sys.setdefaultencoding('utf8')

DEBUG = False
DELAY = 0.1         # delay in seconds between Discourse hits
MAX_TEXT_LEN = 300  # max characters before truncation
SOURCE = "forums"  # the source of each mention in CouchDB
POSTS_PER_PAGE = 20 # Discourse returns posts for a topic in lots of 20 per page

# retrieve URL from config JSON
options = json.load(open('./options.json', 'r'))

#logging configuration
LogLevel = logging.DEBUG # or logging.INFO or logging.WARN, etc.
#LogLevel = logging.INFO # or logging.INFO or logging.WARN, etc.
LogFilename = options['logdir'] + '/forums.log'
LogFormat = '%(asctime)s - %(levelname)s: %(message)s'
#print 'logfile %s, level %s' % (LogFilename, LogLevel)
logging.basicConfig(format=LogFormat,level=LogLevel,filename=LogFilename)

# get tag list from URL
tagurl = options['settings-url']
jsoncontent = urllib.urlopen(tagurl)
reference_tags = json.loads(jsoncontent.read())["tags"]

# Set up the prettyprinter object for debugging
pp = pprint.PrettyPrinter(indent=4)

# create the CouchDB object
couch = couchdb.Server(options['url'])
db = couch[options['db']]

# some other settings.
baseurl = options['forums']['url']
logging.debug('baseurl = %s', pp.pformat(baseurl))
version = '0.2'
headers = {'User-Agent' : 'WEnotes-Forum/%s' % (version)}
#print headers

# create the parser for returned content from Discourse
parser = argparse.ArgumentParser(description='Harvest posts from Discourse Forums.')
parser.add_argument('-f', '--full', action='store_false',
        help='get list of categories, and then every topic in each')
args = parser.parse_args([])

def have_mention(msg_id):
    """Return boolean showing if we already have this message."""
    view = db.view('ids/forumsids')
    have = (len(view[msg_id]) > 0)
    return have

# check a tag list against our reference_tags
def interesting_tags(tags):
    """Return list of interesting tags, or false if none."""
    common_tags = list(set(tags) & set(reference_tags))
    #logging.debug("taglist: %s\nreference tags: %s\ncommon tags: %s", tags, reference_tags, common_tags)
    if common_tags:
        logging.debug("interesting tags: %s", common_tags)
        return common_tags
    else:
        return False

if args.full:
        # get the list of categories
        categories = '%s/categories.json' % (baseurl)
        time.sleep(DELAY)
        r = requests.get(categories, headers=headers, verify=False)
        d = json.loads(r.text)
        cat_list = d['category_list']['categories']
        #logging.debug("categories: %s", json.dumps(cat_list, indent=2, sort_keys=True))
        for cat in cat_list:
            logging.debug("category: %s(%s)", cat['name'], cat['id'])
            topics = '%s/c/%s.json' % (baseurl, cat['id'])
            if DEBUG:
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>>", \
                        cat['id'], cat['name'], cat['slug'], topics
            #logging.debug("category: %s, %s, %s, %s", cat['id'], cat['name'], cat['slug'], topics)
            time.sleep(DELAY)
            r2 = requests.get(topics, headers=headers, verify=False)
            d = json.loads(r2.text)
            topic_list = d['topic_list']['topics']
            #logging.debug("topics: %s", json.dumps(topic_list, indent=2, sort_keys=True))
            for topic in topic_list:
                pages = topic['posts_count']/POSTS_PER_PAGE;
                if (topic['posts_count'] % POSTS_PER_PAGE > 0):
                    #print "adding a page with modulo %f" % (topic['posts_count'] % POSTS_PER_PAGE)
                    pages += 1
                #print "%d pages for %d posts for topic %s" % (pages, topic['posts_count'], topic['title'])
                for page in (1,pages):
                    posts = '%s/t/%s.json?page=%d' % (baseurl, topic['id'], page)
                    logging.debug("topic: %s(%s), page %d", topic['title'], topic['id'], page)
                    #logging.debug("topic %s: ", json.dumps(topic, indent=2, sort_keys=True))
                    #logging.debug('     tags: %s', json.dumps(topic['tags'], indent=2, sort_keys=True))
                    common_tags = interesting_tags(topic['tags'])
                    if not common_tags:
                        logging.debug('no interesting tags')
                        continue
                    time.sleep(DELAY)
                    r3 = requests.get(posts, headers=headers, verify=False)
                    p = json.loads(r3.text)
                    post_list = p['post_stream']['posts']
                    #logging.debug("post_list %s: ", json.dumps(post_list, indent=2, sort_keys=True))
                    if True:
                        for post in post_list:
                            #logging.debug("post %s: ", json.dumps(post, indent=2, sort_keys=True))
                            if post['deleted_at']:
                                continue
                            link = "%s/t/%s/%s" % (baseurl, post['topic_id'], post['post_number'])
                            logging.debug('link: %s', link)
                            if have_mention(link):
                                logging.debug('existing link: %s', link)
                                continue
                            logging.debug('interesting link: %s', link)
                            text = post['cooked'].replace('\n', ' ')
                            text = re.sub(r'<[^>]*?>', ' ', text)   # remove HTML tags
                            text = re.sub(r' {2,}', ' ', text)      # collapse spaces
                            text = topic['fancy_title'].strip() + ': ' + text.strip()
                            truncated = False
                            i = len(text)
                            if i > MAX_TEXT_LEN:
                                i = MAX_TEXT_LEN
                                while text[i] != ' ' and i > 0:
                                    i -= 1
                                text = text[:i] + '...'
                                truncated = True
                            from_user_name = post['display_username']
                            if from_user_name == '':
                                from_user_name = post['username']
                            profile_image_url = post['avatar_template'].replace('{size}', '48')
                            if not re.match(r'^(https?:)?//', profile_image_url):
                                profile_image_url = baseurl + profile_image_url
                            mention = {
                                    'created_at': post['created_at'],
                                    'from_user': post['username'],
                                    'from_user_name': from_user_name,
                                    'id': link,
                                    'post_id': post['id'],
                                    'profile_image_url': profile_image_url,
                                    'profile_url': baseurl + '/users/' + post['username'],
                                    'text': text,
                                    'truncated': truncated,
                                    'we_link': link,
                                    'we_source': SOURCE,
                                    'we_version': version,
                                    'we_tags': common_tags,
                                    'we_timestamp': post['created_at']
                                    }
                            if DEBUG:
                                print json.dumps(mention, indent=2, sort_keys=True)
                            else:
                                logging.info('adding post %s by %s with tag(s): %s', mention['id'], from_user_name, common_tags)
                                db.save(mention)
