{
   "_id": "_design/ids",
   "_rev": "52-cb159457e371f4adaee638dbe2a0da5a",
   "views": {
       "twitter": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"twitter\" && doc.id_str && doc.we_timestamp) {\n    emit(doc.we_timestamp, doc.id_str);\n  }\n}\n"
       },
       "identica": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"identica\" && doc.we_timestamp && doc.id) {\n    if (doc.we_tags) {\n      for (var i=0; i<doc.we_tags.length; i++) {\n        emit([doc.we_tags[i], doc.we_timestamp], doc.id);\n      }\n    } else if (doc.we_tag) {\n      emit([doc.we_tag, doc.we_timestamp], doc.id);\n    }\n  }\n}\n"
       },
       "moodle": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"moodle\" && doc.we_timestamp) {\n    if (doc.we_tags) {\n      for (var i=0; i<doc.we_tags.length; i++) {\n        emit(doc.we_timestamp, doc.we_tags[i]);\n      }\n    } else if (doc.we_tag) {\n      emit(doc.we_timestamp, doc.we_tag);\n    }\n  }\n}\n"
       },
       "ask": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"ask\" && doc.we_timestamp) {\n    emit(doc.we_timestamp, null);\n  }\n}\n"
       },
       "google": {
           "map": "function (doc) {\n  var replies = 0, plusoners = 0, resharers = 0;\n  if (doc.we_source && doc.we_source === \"g+\" && doc.id) {\n    if (doc.object) {\n      if (doc.object.replies) replies = doc.object.replies.totalItems;\n      if (doc.object.plusoners) plusoners = doc.object.plusoners.totalItems;\n      if (doc.object.resharers) resharers = doc.object.resharers.totalItems;\n     }\n    emit(doc.id, [doc._rev, doc.we_tags, replies, plusoners, resharers]);\n  }\n}"
       },
       "identicadups": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"identica\" && doc.we_timestamp && doc.we_tag && doc.id) {\n    emit(doc.id, [doc.we_tag, doc.we_timestamp, doc.text]);\n  }\n}\n"
       },
       "identica_stats": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"identica\" && doc.id) {\n    if (doc.we_tags) {\n      for (var i=0; i<doc.we_tags.length; i++) {\n        emit(doc.we_tags[i], doc.id);\n      }\n    } else if (doc.we_tag) {\n      emit(doc.we_tag, doc.id);\n    }\n  }\n}\n",
           "reduce": "_stats"
       },
       "mastodon": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"mastodon\" && doc.we_timestamp && doc.we_tag && doc.id) {\n    emit(doc.id, [doc.we_tag, doc.we_timestamp, doc.text]);\n  }\n}\n"
       },
       "mastodon_stats": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"mastodon\" && doc.id) {\n    if (doc.we_tags) {\n      for (var i=0; i<doc.we_tags.length; i++) {\n        emit(doc.we_tags[i], doc.id);\n      }\n    } else if (doc.we_tag) {\n      emit(doc.we_tag, doc.id);\n    }\n  }\n}\n",
           "reduce": "_stats"
       },
       "feed": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"feed\" && doc.id && doc.we_tags) {\n    emit(doc.we_link, doc.we_tags);\n  }\n}\n"
       },
       "twitterids": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"twitter\" && doc.id_str && doc.we_timestamp) {\n    emit(doc.id_str, null);\n  }\n}"
       },
       "edem630": {
           "map": "function(doc) {\n  if (doc.we_tags && doc.we_tags.indexOf('edem630') > -1) {\n    if (doc.we_link) {\n      emit(doc.we_link, doc.id);\n    } else {\n      emit(doc.url, doc.id);\n    }\n  }\n}"
       },
       "wikieducator_roots": {
           "map": "function(doc) {\n  if ((doc.we_source === 'wikieducator') && (doc.we_r)) {\n    emit(doc.we_r, doc.we_timestamp);\n  }\n}"
       },
       "groups": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"groups\" && doc.we_timestamp) {\n    if (doc.we_tags) {\n      for (var i=0; i<doc.we_tags.length; i++) {\n        emit(doc.we_timestamp, doc.we_tags[i]);\n      }\n    } else if (doc.we_tag) {\n      emit(doc.we_timestamp, doc.we_tag);\n    }\n  }\n}\n"
       },
       "community": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"community\" && doc.we_timestamp) {\n    if (doc.we_tags) {\n      for (var i=0; i<doc.we_tags.length; i++) {\n        emit(doc.we_timestamp, doc.we_tags[i]);\n      }\n    } else if (doc.we_tag) {\n      emit(doc.we_timestamp, doc.we_tag);\n    }\n  }\n}\n"
       },
       "communityids": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"community\" && doc.we_timestamp && doc.id) {\n    emit(doc.id, null);\n  }\n}\n"
       },
       "forumsids": {
           "map": "function (doc) {\n  if (doc.we_source && doc.we_source === \"forums\" && doc.we_timestamp && doc.id) {\n    emit(doc.id, null);\n  }\n}\n"
       }
   },
   "language": "javascript",
   "bookmarks": null
}
