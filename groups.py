#!/usr/bin/python

# Copyright 2015 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime
import urllib, urllib2
import couchdb
import feedparser
import lxml.html
import sys
import json

DEBUG = False

# memoized dict with keys: profile url
#                  values: dict with keys url, height, width
images = {}

def profile_img(groups, profile):
    """Return a dictionary of image url, height, width

    Scrapes information from web forum."""
    global images
    if not images.has_key(profile):
        # fetch the user profile
        f = groups.open(profile)
        html = lxml.html.parse(f).getroot()
        img_div = html.find_class('userimage')
        pics = img_div[0].find('img')
        attrs = pics.attrib
        img_url = attrs['src']
        if len(img_url)<5 or img_url[:5] <> 'data:':
            img_url = 'http://groups.oeru.org' + img_url
        images[profile] = {'url': img_url}
        if attrs.has_key('height'):
            images[profile]['height'] = attrs['height']
        if attrs.has_key('width'):
            images[profile]['width'] = attrs['width']
    return images[profile]

# retrieve URL including authentication credentials from config JSON
options = json.load(open('./options.json', 'rt'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]

# get the last time for a groups post in the database
view = db.view('ids/groups', descending=True, limit=1)
if len(view) == 1:
    for row in view:
        lasttime = row.key
else:
    lasttime = "2000-01-01T00:00:00.000Z"

feeds = ['http://groups.oeru.org/s/search.atom?t=0&p=1&r=1&l=20']

# we will only fetch "public" posts, so don't bother logging in
groups = urllib2.build_opener()
groups.addheaders = [('User-agent', 'WEnotes-Fetcher/0.1')]

for feed in feeds:
    rss = feedparser.parse(feed)
    #feedtitle = rss['channel']['title']

    items = rss['items']
    items.reverse()

    for item in items:
        if DEBUG:
            print item['updated'], item['author'], item['title']
            print '<<<<<'
            print item['summary']
            print '>>>>>', ord(item['summary'][-1])
            for k,v in item.items():
                print k.rjust(20), v
            print '====='

        if item['title'] == 'RSS Error' and item['description'] == 'Error reading RSS data':
            break
        if item['updated'] <= lasttime and not DEBUG:
            continue
        author = item['author']
        profile_url = item['author_detail']['href']
        img = profile_img(groups, profile_url)

        # try to remove signature blocks from summary
        #  (some posters use a non-standard emdash rather than "-- ")
        summary = item['summary']
        summary_parts = re.split(u" ((--)|\u2014) ", summary, maxsplit=1)
        if len(summary_parts) > 1:
            summary = summary_parts[0]

        # try to remove quoted text
        # FIXME this assumes they are doing evil top posting
        #       by just looking for attribution line and cutting there
        #       and then adding ellipsis whether it is warranted or not
        summary_parts = re.split(u"\sOn ((\w{3}, \w{3} \d{1,2})|(\d{4}-\d\d-\d\d)),.*?wrote:\s*",
                summary, maxsplit=1)
        if len(summary_parts) > 1:
            summary = summary_parts[0] + u"\u2026"

        abridged = item['title'] + ':  ' + summary
        abridged = abridged.replace(u"\u2019", "'")

        # OnlineGroups appears to truncate summaries with unicode ellipsis
        truncated = False
        if abridged[-1] == u"\u2026":
            abridged = abridged[:-1] + '...'
            truncated = True

        mention = {
                'from_user': author,
                'from_user_name': author,
                'created_at': item['updated'],
                'profile_image_url': img['url'],
                'text': abridged,
                'truncated': truncated,
                'id': item['id'],
                'profile_url': profile_url,
                'we_source': 'groups',
                #'we_feed': feedtitle,
                'we_tags': ['oeru'],
                'we_timestamp': item['updated'],
                'we_link': item['link']
                }
        if img.has_key('height'):
            mention['profile_image_height'] = img['height']
        if img.has_key('width'):
            mention['profile_image_width'] = img['width']

        if DEBUG:
            print 'vvvvvvvvvvvvvvvvvv'
            print mention
            print '^^^^^^^^^^^^^^^^^^'
        else:
            db.save(mention)
