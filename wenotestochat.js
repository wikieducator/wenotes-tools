/*
 * WEnotes -> RocketChat
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
/* jshint node:true, esversion:6 */
"use strict";

var util = require('util'),
    Faye = require('faye'),
    request = require('request'),
    options = require('./options.json');

var client = new Faye.Client(options.fayeurl),
    sub;

sub = client.subscribe(options.WEnotes.RocketChat.subscribe, (m) => {
  var notification, icon, tags, attachment,
      // ?? || ?? || mastodon
      username = m.from_user_name || m.from_user || m.account.username,
      entities = {
        amp: '&',
        lt: '<',
        gt: '>',
        quot: '"'
      },
      entityRegex = RegExp(`&(${Object.keys(entities).join('|')});`, 'g');

  function HTMLEntityDecode(s) {
    return s.replace(entityRegexp, (match, p1) => entities[p1]);
  }

  // if it is not a deleted message, post it to Rocket.Chat
  // and it is not a g+ +1
  if (!m.we_d && !(m.we_source == 'g+' && m.kind == 'plus#activity')) {
    // ?? || ?? || mastodon
    icon = m.profile_image_url_https || m.profile_image_url || m.account.avatar;
    if (!icon && m.gravatar) {
      icon = `https://www.gravatar.com/avatar/${m.gravatar}?s=48&d=identicon`;
    }
    tags = m.we_tags.map((x) => '#' + x).join(',');
    notification = {
      channel: options.WEnotes.RocketChat.channel,
      username: `${username}@${m.we_source} (${tags})`,
      icon_url: icon,
      text: m.text
    };

    switch(m.we_source) {
      case 'g+':
        if (m.truncated) {
          attachment = {
            title: 'g+',
            title_link: m.we_link,

            author_name: username,
            author_link: m.profile_url,
            author_icon: icon
          };
          notification.attachments = [attachment];
        }
        break;
      case 'moodle':
        if (m.truncated) {
          attachment = {
            // assumes the "title: body" in WEnotes
            title: 'Moodle post',
            title_link: m.we_link,

            author_name: username,
            author_link: m.profile_url,
            author_icon: icon
          };
          notification.attachments = [attachment];
        }
        break;
      case 'groups':
      case 'community':
      case 'forums':
      case 'feed':
        attachment = {
          // assumes the "title: body" in WEnotes
          title: m.text.replace(/:.*/, ''),
          title_link: m.we_link,

          author_name: username,
          author_link: m.profile_url,
          author_icon: icon
        };
        notification.attachments = [attachment];
        break;
      case 'mastodon':
        attachment = {
          // assumes the "title: body" in WEnotes
          title: m.content,
          title_link: m.url,

          author_name: username,
          author_link: m.account.url,
          author_icon: icon
        };
        notification.attachments = [attachment];
        break;
    }

    request.post({
      uri: options.WEnotes.RocketChat.webhook,
      json: notification
    }, (error, response, body) => {
      if (error) {
        console.log(`WEnotes->Rocket.Chat POST error: ${error} response: ${util.inspect(response)} body: ${util.inspect(body)}`);
      }
    });
  }
});
