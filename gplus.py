#!/usr/bin/python

# Copyright 2012-2016 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import urllib
import urllib2
import time
import couchdb
import json
from operator import itemgetter

# retrieve URL including authentication credentials from config JSON
options = json.load(open('./options.json', 'rt'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]

couch2 = couchdb.Server(options['localcouch'])
dbfeeds = couch2[options['dbfeeds']]

tags = options['tags']
communities = options['communities']

gplus_search = 'https://www.googleapis.com/plus/v1/activities'
gplus_activities_list = "https://www.googleapis.com/plus/v1/people/%s/activities/public"

def abridge(s):
    "return possibly truncated string and truncated flag"
    truncated = False
    # strip out HTML markup before abridging, so we don't stop midtag
    abridged = re.sub(r'<[^>]*>', ' ', s)
    abridged = re.sub(r'\s*by [^.]+\.\n?', '', abridged)
    abridged = abridged.replace('&nbsp;', ' ')
    abridged = abridged.replace('&#39;', "'")
    abridged = abridged.replace('\n', ' ')
    abridged = abridged.strip()
    # get rid of multiple spaces (which the above may have introduced)
    abridged = re.sub(r'  +', ' ', abridged)
    i = len(abridged)
    if i > 280:
        i = 280
        while abridged[i] != ' ' and i > 0:
            i -= 1
        abridged = abridged[:i] + '...'
        truncated = True
    return (abridged, truncated)

def getitems(url, query={}, person=None):
    items = []
    query['key'] = options['googleapikey']
    if person:
        url = url % (person)
    while 1:
        d = json.loads(urllib2.urlopen(url + '?' + urllib.urlencode(query)).read())
        if d.has_key('items'):
            items += d['items']
        if not d.has_key('nextPageToken') or (d.has_key('items') and len(d['items']) <= 0):
            return items
        query['pageToken'] = d['nextPageToken']
        time.sleep(1)

def formatitem(item, tag):
    doc = item.copy()
    doc['we_source'] = 'g+'
    doc['we_tags'] = [tag]
    doc['we_timestamp'] = item['published']
    doc['from_user'] = item['actor']['displayName']
    doc['from_user_name'] = item['actor']['displayName']
    doc['profile_image_url'] = item['actor']['image']['url']
    doc['profile_url'] = item['actor']['url']
    if not doc.has_key('text'):
        doc['text'], truncated = abridge(item['object']['content'])
        if truncated:
            doc['truncated'] = true
    # make a consistent we_link to thread
    if doc.has_key('url'):
        doc['we_link'] = doc['url']
        del doc['url']
    else:
        doc['we_link'] = item['inReplyTo'][0]['url']
    return doc

def processitems(items, tag, get_replies = True):
    time.sleep(1)
    # loop through all of the items
    # if we don't have it, save it
    # if the reply count has changed, process those items
    for item in items:
        view = db.view('ids/google', key=item['id'], include_docs=True,
                limit=1)
        if len(view) == 1:
            update = False
            # check if comments, plusoners, or resharers have changed
            # or if we are missing this tag
            for row in view:
                doc = row.doc
                newobj = item['object']
                dbobj = doc['object']
                if (newobj.has_key('plusoners') and newobj['plusoners']['totalItems'] <> dbobj['plusoners']['totalItems']) \
                or (newobj.has_key('resharers') and newobj['resharers']['totalItems'] <> dbobj['resharers']['totalItems']):
                    update = True
                if newobj.has_key('replies') and newobj['replies']['totalItems'] <> dbobj['replies']['totalItems']:
                    update = True
                    processitems(getitems(newobj['replies']['selfLink']), tag)
                if tag not in doc['we_tags']:
                    doc['we_tags'].append(tag)
                    update = True
            if update:
                doc['object']['plusoners'] = newobj['plusoners'].copy()
                doc['object']['resharers'] = newobj['resharers'].copy()
                doc['object']['replies'] = newobj['replies'].copy()
                # update CouchDB
                db[row['id']] = doc
        else:
            doc = formatitem(item, tag)
            db.save(doc)
            if get_replies and item['object'].has_key('replies') and item['object']['replies']['totalItems'] > 0:
                processitems(getitems(item['object']['replies']['selfLink']), tag)
    return

#   for each activity
#       if we have it in db
#           get the we_tags
#           if pluses count changed
#               update it
#           if comments count changed
#               update it
#               process comments using current we_tags
#       else
#           scan the text looking for we_tags
#           save
#           process comments using current we_tags
def plusSearch(tag):
    """search for all activities for this tag"""
    query = {'query': '#' + tag,
            'maxResults': 20}
    items = getitems(gplus_search, query)
    for item in items:
        if item['kind'] <> u'plus#activity':
            print "unknown item kind tag: %s kind: %s" % (tag, item['kind'])
        view = db.view('ids/google', key=item['id'], include_docs=True,
                limit=1)
        if len(view) == 1:
            for row in view:
                #for k,v in row.doc.items():
                #    print "%20s: %s" % (k, v)
                doc = row.doc
                o = item['object']
                od = doc['object']
                update = False
                replies = False
                if od['plusoners']['totalItems'] <> o['plusoners']['totalItems']:
                    update = True
                    doc['object']['plusoners'] = o['plusoners'].copy()
                if od['resharers']['totalItems'] <> o['resharers']['totalItems']:
                    update = True
                    doc['object']['resharers'] = o['resharers'].copy()
                if od['replies']['totalItems'] <> o['replies']['totalItems']:
                    update = True
                    doc['object']['replies'] = o['replies'].copy()
                    replies = True
                if update:
                    db[row['id']] = doc
                if replies:
                    replies(item['replies']['selflink'], item['we_tags'])
        else:
            print "---news to me"

def main():
    for tag in tags:
        items = getitems(gplus_search,
                         {'maxResults': 20,
                             'query': '#' + tag})
        # sort them chronologically
        items = sorted(items, key=itemgetter('published'))
        processitems(items, tag)
        time.sleep(2)
    # get communities and "g+ as blog" from feed table
    for feed in dbfeeds.view('feed/activegplus', include_docs=True):
        print "activegplus", feed
        id = feed.doc['url']
        # skip things that don't seem to have a tag
        if len(feed.doc['we_tags']) < 1:
            continue
        tag = feed.doc['we_tags'][0]
        print "  tag:", tag
        items = getitems(gplus_activities_list, {'maxResults': 100}, id)

        # sort them chronologically
        items = sorted(items, key=itemgetter('published'))
        print "  items:"
        print items
        print '======'
        if 0:
            for item in items:
                ob = item['object']
        if 0:
            kinds = {}
            for item in items:
                kind = item['kind']
                if kinds.has_key(kind):
                    kinds[kind] += 1
                else:
                    kinds[kind] = 1
                replies = item['object']['replies']['totalItems']
                print item['id'], replies, item['object']['replies']['selfLink']
            print kinds
        processitems(items, tag,
               get_replies = (feed.doc['type'] <> 'gplusblog'))
        time.sleep(2)

if __name__ == "__main__":
    main()
