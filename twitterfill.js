/*jshint strict:false, node:true */
/**
    poll Twitter to fill in any missed tweets
    @author Jim Tittsler <jim@OERfoundation.org>
    @license MIT
 **/

var couch = require('couch-client'),
    options = require('./options.json'),
    oauth = require('oauth');

var mentionsdb = couch(options.url + '/' + options.db);
var tags = options.tags.map(function(t) { return '#' + t; });
console.log(tags);
var results = [],
    regexps = [];
var processCount = 0;

function checkAndSave() {
  if (results.length === 0) {
    return;
  }

  processCount = processCount + 1;
  console.log('======================', processCount, '==================');
  console.log('results.length', results.length);
  var r = results.pop(),
      id_str = r.id_str,
      hashtags = r.entities.hashtags,
      hashtagsl = hashtags.length,
      thistags = [],
      i;
  for (i=0; i<hashtagsl; i++) {
    thistags.push(hashtags[i].text);
  }
  console.log('check for ' + id_str + ' ' + r.created_at + ' ' + thistags.join(' '));
  mentionsdb.view('/mentions/_design/ids/_view/twitterids',
    {key: id_str, limit: 1}, function(err, doc) {
      var i, atag, we_tags = [];
      if (err) {
        console.log('***error fetching ' + id_str, err);
        throw err;
      } else {
        if (doc.rows.length === 0) {
          console.log('+++missing id', doc.rows);

          // keep just the part of the user record we need
          r.from_user = r.user.screen_name;
          r.from_user_name = r.user.name;
          r.from_user_id = r.user.id;
          r.from_user_id_str = r.user.id_str;
          r.profile_image_url = r.user.profile_image_url;
          r.profile_image_url_https = r.user.profile_image_url_https;
          delete r.user;

          var d = new Date(r.created_at);
          r.we_timestamp = JSON.stringify(d).replace(/"/g, '');
          r.we_source = 'twitter';
          console.log(' tags', tags, ' hashtags', hashtags);
          for (i=0; i<hashtags.length; i++) {
            atag = hashtags[i].text.toLowerCase();
            if (options.tags.indexOf(atag) !== -1) {
              we_tags.push(hashtags[i].text.toLowerCase());
            } else if (options.blacklist.indexOf(atag) >= 0) {
              // don't bother saving tweets containing blacklisted tags
              return;
            }
          }
          r.we_tags = we_tags;
          console.log('>>>>we_tags', r.we_tags);
          // get a deep copy of r
          mentionsdb.save(JSON.parse(JSON.stringify(r)));
        }
      }
      process.nextTick(checkAndSave);
    });
}

function getTweets() {
  "use strict";
  var i, query;

  for (i=0; i<tags.length; i++) {
    regexps[i] = new RegExp(tags[i], "i");
  }
  query = encodeURIComponent(tags.join(' OR '));

  oauth = new oauth.OAuth(
    'https://api.twitter.com/oauth/request_token',
    'https://api.twitter.com/oauth/access_token',
    options.twitter.consumer_key,
    options.twitter.consumer_secret,
    '1.0A',
    null,
    'HMAC-SHA1'
  );

  oauth.get(
    'https://api.twitter.com/1.1/search/tweets.json?q=' + query + '&count=100&result_type=recent&include_entities=true&since_id=281900803814457347',
    options.twitter.access_token_key,
    options.twitter.access_token_secret,
    function(e, data) {
      var r;
      if (e) console.log('oauth.get error: ', e);
      try {
        r = JSON.parse(data);
      } catch(err) {
        console.log('*** Unable to parse body', err);
        console.log(data);
        throw(err);
      }
      if (r.statuses) {
        results = r.statuses;
        checkAndSave();
      }
    });
}

// get recent tweets, save those not already in our db
getTweets();
