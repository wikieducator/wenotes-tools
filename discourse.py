#;!/usr/bin/python

# Copyright 2015 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
import couchdb
import json
import argparse
import requests
# for debugging
import logging
import pprint
# to deal with nasty characters included in Hypothesis quotes
import sys
reload(sys)
sys.setdefaultencoding('utf8')

DEBUG = False
DELAY = 1.0         # delay in seconds between Discourse hits
MAX_TEXT_LEN = 300  # max characters before truncation
SOURCE = "community"  # the source of each mention in CouchDB
POSTS_PER_PAGE = 20 # Discourse returns posts for a topic in lots of 20 per page

# retrieve URL from config JSON
options = json.load(open('./options.json', 'r'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]
baseurl = options['discourse']
headers = {'User-Agent' : 'WEnotes-Discourse/0.2'}

#logging configuration
LogLevel = logging.DEBUG # or logging.INFO or logging.WARN, etc.
#LogLevel = logging.INFO # or logging.INFO or logging.WARN, etc.
LogFilename = options['logdir'] + '/community.log'
LogFormat = '%(asctime)s - %(levelname)s: %(message)s'
#print 'logfile %s, level %s' % (LogFilename, LogLevel)
logging.basicConfig(format=LogFormat,level=LogLevel,filename=LogFilename)

parser = argparse.ArgumentParser(description='Harvest posts from Discourse by Category.')
parser.add_argument('-f', '--full', action='store_false',
        help='get list of categories, and then every topic in each')
args = parser.parse_args([])

def have_mention(msg_id):
    """Return boolean showing if we already have this message."""
    view = db.view('ids/communityids')
    have = (len(view[msg_id]) > 0)
    return have

def last_time():
    """Return the last time for a community post in the database."""
    view = db.view('ids/community', descending=True, limit=1)
    if len(view) == 1:
        for row in view:
            lasttime = row.key
    else:
        lasttime = "2000-01-01T00:00:00.000Z"
    return lasttime

if args.full:
    # get the list of categories
    categories = '%s/categories.json' % (baseurl)
    time.sleep(DELAY)
    r = requests.get(categories, headers=headers, verify=False)
    d = json.loads(r.text)
    if DEBUG:
        print json.dumps(d, indent=4, sort_keys=True)
    cat_list = d['category_list']['categories']
    for cat in cat_list:
        topics = '%s/c/%s.json' % (baseurl, cat['id'])
        if DEBUG:
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>", \
                    cat['id'], cat['name'], cat['slug'], topics
        time.sleep(DELAY)
        r2 = requests.get(topics, headers=headers, verify=False)
        d = json.loads(r2.text)
        topic_list = d['topic_list']['topics']
        for topic in topic_list:
            pages = topic['posts_count']/POSTS_PER_PAGE;
            if (topic['posts_count'] % POSTS_PER_PAGE > 0):
                #print "adding a page with modulo %f" % (topic['posts_count'] % POSTS_PER_PAGE)
                pages += 1
            #print "%d pages for %d posts for topic %s" % (pages, topic['posts_count'], topic['title'])
            for page in (1,pages):
                posts = '%s/t/%s.json?page=%d' % (baseurl, topic['id'], page)
                logging.debug("topic: %s(%s), page %d", topic['title'], topic['id'], page)
                time.sleep(DELAY)
                r3 = requests.get(posts, headers=headers, verify=False)
                p = json.loads(r3.text)
                post_list = p['post_stream']['posts']
                for post in post_list:
                    if post['deleted_at']:
                        continue
                    link = "%s/t/%s/%s" % (baseurl, post['topic_id'],
                            post['post_number'])
                    if have_mention(link):
                        continue
                    text = post['cooked'].replace('\n', ' ')
                    text = re.sub(r'<[^>]*?>', ' ', text)   # remove HTML tags
                    text = re.sub(r' {2,}', ' ', text)      # collapse spaces
                    text = topic['fancy_title'].strip() + ': ' + text.strip()
                    truncated = False
                    i = len(text)
                    if i > MAX_TEXT_LEN:
                        i = MAX_TEXT_LEN
                        while text[i] != ' ' and i > 0:
                            i -= 1
                        text = text[:i] + '...'
                        truncated = True
                    from_user_name = post['display_username']
                    if from_user_name == '':
                        from_user_name = post['username']
                    profile_image_url = post['avatar_template'].replace('{size}', '48')
                    if not re.match(r'^(https?:)?//', profile_image_url):
                        profile_image_url = baseurl + profile_image_url
                    mention = {
                            'created_at': post['created_at'],
                            'from_user': post['username'],
                            'from_user_name': from_user_name,
                            'id': link,
                            'post_id': post['id'],
                            'profile_image_url': profile_image_url,
                            'profile_url': baseurl + '/users/' + post['username'],
                            'text': text,
                            'truncated': truncated,
                            'we_link': link,
                            'we_source': SOURCE,
                            'we_tags': ['oeru'],
                            'we_timestamp': post['created_at']
                            }
                    if DEBUG:
                        print json.dumps(mention, indent=2, sort_keys=True)
                    else:
                        db.save(mention)
