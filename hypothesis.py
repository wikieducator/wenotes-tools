#!/usr/bin/python

""" Harvest hypothesis feed for tags specified in options file."""

# Copyright 2017 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime, timedelta
import couchdb
import urllib, urllib2
import json
import feedparser
import argparse
import requests
import HTMLParser
import hashlib
# for debugging
import logging
import pprint
# to deal with nasty characters included in Hypothesis quotes
import sys
reload(sys)
sys.setdefaultencoding('utf8')


# set header this app will report itself as
headers = {'User-Agent' : 'WEnotes-Hypothesis/0.1'}

# retrieve URL from config JSON
options = json.load(open('./options.json', 'r'))

# Change the database? False for no, usually when debugging
DryRun = False

#logging configuration
#LogLevel = logging.DEBUG # or logging.INFO or logging.WARN, etc.
LogLevel = logging.INFO # or logging.INFO or logging.WARN, etc.
LogFilename = options['logdir'] + '/hypothesis.log'
LogFormat = '%(asctime)s - %(levelname)s: %(message)s'
print 'logfile %s, level %s' % (LogFilename, LogLevel)
logging.basicConfig(format=LogFormat,level=LogLevel,filename=LogFilename)

# database configuration
couch = couchdb.Server(options['url'])
db = couch[options['db']]

# get tag list from URL
tagurl = options['settings-url']
jsoncontent = urllib.urlopen(tagurl)
reference_tags = json.loads(jsoncontent.read())["tags"]

# the URL of the hypothesis RSS feed
hypothesis_url = options["hypothesis"]["url"]
hypothesis_rss_url = hypothesis_url + '/stream.rss?tags='
hypothesis_rss_url_user = hypothesis_url + '/stream.rss?user='

# for parsing HTML in bookmark text
h = HTMLParser.HTMLParser()

# length of content for messages
message_length = 200;

lasttime = "2000-01-01T00:00:00.000Z"

# parse the RSS feed from Hypothesis
parser = argparse.ArgumentParser(description='Harvest posts from Hypothes.is, global web annotation.')
parser.add_argument('-f', '--full', action='store_false',
        help='get list of categories, and then every topic in each')
args = parser.parse_args([])

# Set up the prettyprinter object for debugging
pp = pprint.PrettyPrinter(indent=4)

# get all hypothesis mentions already in our database
# (so we can check if ones we find are already held)
all_mentions = {}
all_mention_ids = db.view('ids/hypothesis')
if len(all_mention_ids):
    for row in all_mention_ids:
        try:
            logging.debug('looking for id %s', row['id'])
            result = db.get(row['id'])
            all_mentions[result['id']] = row['id']
        except:
            logging.exception('failed to get valid response from db looking for id %s', row['id'])
else:
    logging.debug('no previous hypothesis mentions!')

# get the last ID for each of the tags
#stats = db.view('ids/hypothesis_stats', group=True)
#last = {}
#logging.debug("len = %d", len(stats))
#if len(stats) > 0:
#    for row in stats:
#        last[row.key] = row.value['max']

def have_mention(item_id, tag):
    """Return boolean showing if we already have this message."""
    logging.debug('item_id = %s', item_id)
    try:
        if all_mentions[item_id]:
            logging.debug('Found id %s', item_id)
            # now check if this is the same tag as previously associated
            try:
                mention = db.get(all_mentions[item_id])
                try:
                    if mention['we_tags']:
                        logging.debug('we_tags = %s', mention['we_tags'])
                        if tag in mention['we_tags']:
                            logging.debug('we already have this mention with tag %s', tag)
                        else:
                            logging.debug('%s is a new tag for this mention - adding', tag)
                            # provide the document id, not the hypothesis id, and an array of tags
                            add_tag_to_mention(all_mentions[item_id], tag)
                except KeyError:
                    logging.debug('no we_tags defined')
            except:
                logging.exception('failed to retrieve mention %s', all_mentions[item_id])
            return True
    except KeyError:
        logging.debug('failed to find %s', item_id)
        return False

# add a tag or tags to the existing tags on a hypothesis mention,
# removing we_tag if set, populating we_tags with any existing tag(s) and
# any new tags
def add_tag_to_mention(id, tag):
    # get the mention
    mention = db.get(id)
    mention['we_tags'].append(tag)
    mention['we_tags'] = unique_tags(mention['we_tags'])
    mention['we_tags'] = sort_tags(mention['we_tags'])
    new_id, rev = db.save(mention)
    logging.debug('updated mention %s (%s), added tag %s', id, new_id, tag)

# ensure there aren't any duplicate tags
# https://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-whilst-preserving-order
def unique_tags(tags):
    seen = set()
    seen_add = seen.add
    return [tag for tag in tags if not (tag in seen or seen_add(tag))]

# sort tags in alphabetical order
# https://stackoverflow.com/questions/10269701/case-insensitive-list-sorting-without-lowercasing-the-result
def sort_tags(tags):
    return sorted(tags, key=lambda s: s.lower())

# deal with the +0000 time offset, not supported by datetime
# see https://stackoverflow.com/questions/23940551/why-z-is-not-supported-by-pythons-strptime
def dt_parse(t):
    ret = datetime.strptime(t[0:24], '%a, %d %b %Y %H:%M:%S')
    if t[26]=='+':
        ret += timedelta(hours=int(t[27:30]))
    elif t[26]=='-':
        ret -= timedelta(hours=int(t[27:30]))
    return ret

def abridge(text, link):
    logging.debug('mention text: %s', pp.pformat(text))
    # initialise
    truncated = False
    # pull out "blockquote" tags and replace with quotes
    abridged = re.sub(r'<blockquote>', '', text)
    abridged = re.sub(r'</blockquote>', '... ', abridged)
    # pull out all the html tags
    abridged = re.sub(r'<[^>]*>', '', abridged)
    # remove any escaped tags
    abridged = h.unescape(abridged)
    # remove square brackets (link anchors)
    abridged = re.sub(r'\[|]', ' ', abridged)
    # remove | char
    abridged = re.sub(r'\|', '', abridged)
    # remove multiple spaces
    abridged = re.sub(r'\s+', ' ', abridged)
    # remove line feeds and non-breaking spaces
    abridged = abridged.replace('&nbsp;', ' ')
    abridged = abridged.replace('\n', ' ')
    # abridge to message_length characters + ... (i.e. 3)
    # + 'Link added - ' (i.e. 13)
    # + url of max 32 char, or 48
    i = len(abridged)
    if i > (message_length - 48):
        i = (message_length - 48)
        while abridged[i] != ' ' and i > 0:
            i -= 1
        abridged = abridged[:i] + '...'
        truncated = True
    # prepend link:
    abridged = 'Annotation: ' + link + ' - ' + abridged
    return (abridged, truncated)

def save_mentions(tag, reference_tags):
    mentions = {}
    # find all of hypothesis
    rss = feedparser.parse(hypothesis_rss_url + tag)

    # find the channel title
    feedtitle = rss['channel']['title']

    items = rss['items']
    # reverse them, so oldest is first
    items.reverse()

    # for each item in RSS check if it has one (or more) of our tags
    for item in items:
        # is this an error item? If so, bail
        if item['title'] == 'RSS Error':
            break
        #
        dt = dt_parse(item['published'])
        we_timestamp = dt.strftime('%Y-%m-%dT%H:%M:%S.000Z')
        #print we_timestamp
        if we_timestamp <= lasttime:
            continue
        seconds = time.mktime(dt.timetuple())
        # check if we've seen the gid before...
        if have_mention(item['id'], tag):
            continue
        # strip out HTML markup before abridging, so we don't stop midtag
        body = item['title'] + ' - ' + item['summary']
        (abridged, truncated) = abridge(body, item['link'])
        # get author's name
        author = item['author_detail']['name']
        # username
        username = item['author']
        # create the mention object
        mention = {
            'user': {
                'name': author,
                'username': username,
                'feed_url': hypothesis_rss_url_user + username,
                'profile_url': hypothesis_url + '/users/' + username
            },
            'from_user_name': author,
            'created_at': item['published'],
            'text': abridged,
            'truncated': truncated,
            'id': item['id'],
            #'profile_url': item['author'],
            'we_source': 'hypothesis',
            'we_feed': '%s' % (feedtitle),
            'we_tags': [tag],
            'we_timestamp': we_timestamp,
            'we_link': item['link']
        }
        logging.debug(pp.pformat(mention))
        logging.info('adding %s', item['id'])
        logging.info('===========')
        if not DryRun:
            logging.info('writing mention %s', item['id'])
            try:
                id, rev = db.save(mention)
                logging.debug('id = %s, rev = %s', id, rev)
                # add to all_mentions to avoid getting this one again
                all_mentions[item['id']] = id
            except:
                logging.exception('save of item %s failed!', item['id'])

# build a dictionary, indexed by ID so we can quickly merge we_tags
for tag in reference_tags:
    try:
        logging.debug('trying tag %s', tag)
        # harvest any new mentions for a given tag
        save_mentions(tag, reference_tags)
    except urllib2.HTTPError:
        logging.exception("hypothesis: unable to fetch tag %s", tag)
        pass
logging.info('Done.\n')
