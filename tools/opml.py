#!/usr/bin/python

## create an OPML file for the registered blogs in the course
#
# 20130710 jim@OERfoundation.org
#
# License: MIT

import re
import os
import json
from datetime import datetime
import couchdb
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.dom.minidom

dest = '/home/www/oeru/ucan/EDEM630/blogs.xml'

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['url'])
dbfeeds = couch[options['dbfeeds']]
tag = 'EDEM630'
tag_specific = True    # make tag-specific feeds?

def prettify(x):
    xm = xml.dom.minidom.parseString(tostring(x))
    return xm.toprettyxml()

def children(parent, elements):
    for (e, t) in elements:
        el = SubElement(parent, e)
        el.text = t

now = datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")

root = Element('opml')
root.set('version', '2.0')

head = SubElement(root, 'head')
children(head, [
    ('title', 'UCan EDEM630 Blogs'),
    ('dateCreated', now)
    ])

body = SubElement(root, 'body')
blogs = SubElement(body, 'outline')
blogs.set('text', 'EDEM630 Blogs')
#blogs.set('title', 'EDEM630 Blogs')

active = dbfeeds.view('feed/activebytag', include_docs=True)
for feed in active[tag.lower()]:
    b = SubElement(blogs, 'outline')
    b.set('text', feed.doc['from_user_name'])
    b.set('type', 'rss')
    if (feed.doc['type'] == 'gplus'):
        url = 'http://gplus-to-rss.appspot.com/rss/%s' % feed.doc['url']
    else:
        url = feed.doc['url']
        # use special case knowledge to find tag specific feeds
        if tag_specific:
            if re.search(r'\.((blogger)|(blogspot))\.co', url):
                if url[-1] <> '/':
                    url += '/'
                url = url + '-/%s/' % tag
            else:
                mo = re.match(r'(?P<site>.*?\.wordpress\.com/)feed.*', url)
                if mo:
                    url = mo.group('site') + 'tag/%s/feed' % tag
    b.set('xmlUrl', url)
    b.set('htmlUrl', feed.doc['web'])

print prettify(root)
open(dest, 'wt').write('<?xml version="1.0" encoding="utf-8"?>\n'
        + tostring(root))

