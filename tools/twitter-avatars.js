/*
 * update cached Twitter avatars
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
"use strict";

var util = require('util'),
    cradle = require('cradle'),
    Twitter = require('twitter'),
    request = require('request'),
    options = require('../options.json');

var twitter = new Twitter(options.twitter);

// split http://user:pass@host:port into a map of its parts
function getConnectionMap(cs) {
  var r = {},
      parts = cs.split('@');
  if (parts.length === 2) {
    let userpass = parts[0].replace(/https?:\/\//, '').split(':');
    if (userpass.length === 2) {
      r.user = userpass[0];
      r.pass = userpass[1];
    }
    parts = parts[1].split(':');
    if (parts.length === 2) {
      r.host = parts[0];
      r.port = parts[1];
    }
  }
  return r;
}

var cdb = getConnectionMap(options.url),
    couch = new(cradle.Connection)(cdb.host, cdb.port, {
      auth: {username: cdb.user, password: cdb.pass}}),
    db = couch.database(options.db);

var avatars = {}, avatarURL,
    twitterUser, twitterUsers, userInfo,
    updates;

function processUpdates() {
  var res, doc;
  if (updates.length === 0) {
    process.nextTick(processUsers);
    return;
  }
  res = updates.pop();
  doc = res.doc;
  doc.profile_image_url = userInfo.profile_image_url;
  doc.profile_image_url_https = userInfo.profile_image_url_https;
  doc.from_user_id_str = userInfo.id_str;
  db.save(doc._id, doc._rev, doc, (err, res) => {
    if (err) {
      console.error('Unable to update CouchDB');
    }
    process.nextTick(processUpdates);
  });
}

function getAvatar() {
  twitter.get('users/show',
      {screen_name: twitterUser, include_entities: false},
      (err, user) => {
        if (err) {
          console.error(`error, unable to fetch Twitter user ${twitterUser}`);
          process.nextTick(processUsers);
        } else {
          userInfo = user;
          db.view('user/twitteravatar', {key: twitterUser, include_docs: true}, (err, res) => {
            if (err) {
              console.error(`CouchDB unable to fetch ${twitterUser} for update.`);
              process.nextTick(processUsers);
            } else {
              updates = res;
              process.nextTick(processUpdates);
            }
          });
        }
  });
}

// work through twitterUsers list until empty
function processUsers() {
  if (twitterUsers.length === 0) {
    return;
  }
  twitterUser = twitterUsers.pop();
  avatarURL = avatars[twitterUser];
  if (avatarURL.indexOf('https://') === -1) {
    setTimeout(getAvatar, 5500);
  } else {
    // try to fetch the image
    request(avatarURL, (error, response, body) => {
      if (error) {
        setTimeout(getAvatar, 5500);
      } else {
        setTimeout(processUsers, 1000);
      }
    });
  }
}

// get a list of all unique Twitter users
//   with a map of user: avatarURL
db.view('user/twitteravatar', function(err, res) {
  if (err) {
    console.error('CouchDB view user/twitteravatar error', err);
    util.exit(1);
  } else {
    res.forEach((key, value, id) => {
      if (avatars.hasOwnProperty(key)) {
        // if there are multiple values, fetch truth
        if (avatars[key] !== value) {
          avatars[key] = '';
        }
      } else {
        avatars[key] = value;
      }
    });
    twitterUsers = Object.keys(avatars);
    console.log(`${twitterUsers.length} distinct keys`);
    process.nextTick(processUsers);
  }
});

