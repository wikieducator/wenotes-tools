#!/usr/bin/python

## update Google spreadsheet with user participation metrics
#  derived from the WEnotes aggregate feed
#
# 2013      jim@OERfoundation.org

from __future__ import print_function
import re
import os
import sys
import json
import urllib2
import ConfigParser
import gdata.spreadsheet.service
import couchdb

VERSION = '0.0.2'

if len(sys.argv) <> 2:
    sys.exit("Usage: user-outputs.py SPREADSHEET_KEY")
gskey = sys.argv[1]

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['localcouch'])
db = couch['mentions']

# outputs is a dictionary indexed by user name
#     of dictionaries of source: count
outputs = {}
for row in db.view('user/postcount', group_level=2):
    source = row.key[0]
    user = row.key[1]
    if outputs.has_key(user):
        outputs[user][source] = row.value
    else:
        outputs[user] = {source: row.value}
print(outputs)
print('---------------------------------------------------')

# get login credentials from ~/.wikieducator.rc
config = ConfigParser.SafeConfigParser()
config.read(os.path.expanduser('~/.wikieducator.rc'))
try:
    guser = config.get('google', 'user')
    gpassword = config.get('google', 'password')
except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
    print("missing user/password in ~/.wikieducator.rc", file=sys.stderr)

# read the existing spreadsheet
gs = gdata.spreadsheet.service.SpreadsheetsService()
gs.email = guser
gs.password = gpassword
gs.source = 'user_outputs'
gs.ProgrammaticLogin()
gsws = 'od6'

for entry in gs.GetListFeed(gskey, gsws).entry:
    sr = dict(zip(entry.custom.keys(),
        [value.text for value in entry.custom.values()]))

    changed = False

    # blogposts and WEnotes are indexed by wikiname
    wikiname = sr['wikiname']
    if outputs.has_key(wikiname):
        uouts = outputs[wikiname]
        if uouts.has_key('feed'):
            sr['blogposts'] = str(uouts['feed'])
            changed = True
        if uouts.has_key('wikieducator'):
            sr['wenotes'] = str(uouts['wikieducator'])
            changed = True
    # twitter
    if sr['twitter'] is not None:
        twitterhandle = sr['twitter'].lower()
        if outputs.has_key(twitterhandle):
            sr['tweets'] = str(outputs[twitterhandle]['twitter'])
            changed = True
    # g+
    if sr['gplus'] is not None:
        mo = re.search(r'(?P<gpid>\d{20,21})', sr['gplus'])
        if mo:
            gpid = mo.group('gpid')
            if outputs.has_key(gpid):
                sr['gposts'] = str(outputs[gpid]['g+'])
                changed = True
    if changed:
        gs.UpdateRow(entry, sr)

