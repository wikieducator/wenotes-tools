/*
 * convert profile_image_url(_https) that reference gravatar
 * to just d.gravatar entries
 *  - saves storage
 *  - allows client to easily choose SSL for fetches
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
"use strict";

var util = require('util'),
    cradle = require('cradle'),
    options = require('../options.json');

var updates = [],
    updateCount = 0;

// split http://user:pass@host:port into a map of its parts
function getConnectionMap(cs) {
  var r = {},
      parts = cs.split('@');
  if (parts.length === 2) {
    let userpass = parts[0].replace(/https?:\/\//, '').split(':');
    if (userpass.length === 2) {
      r.user = userpass[0];
      r.pass = userpass[1];
    }
    parts = parts[1].split(':');
    if (parts.length === 2) {
      r.host = parts[0];
      r.port = parts[1];
    }
  }
  return r;
}

var cdb = getConnectionMap(options.url),
    couch = new(cradle.Connection)(cdb.host, cdb.port, {
      auth: {username: cdb.user, password: cdb.pass}}),
    db = couch.database(options.db);

function processUpdates() {
  var id;
  if (updates.length === 0) {
    return;
  }
  id = updates.pop();
  db.get(id, (err, doc) => {
      if (err) {
        console.log(`Error: unable to fetch ${id}`);
        process.nextTick(processUpdates);
      } else {
        let gravatarURL = doc.profile_image_url_https || doc.profile_image_url,
            gravatar = /.*gravatar\.com\/avatar\/([^?]+)/.exec(gravatarURL)[1];
        updateCount += 1;
        console.log('=================================================');
        console.log(`${updateCount} ${id}`);
        console.log('  url ', gravatarURL);
        console.log('  hash', gravatar);
        doc.gravatar = gravatar;
        delete doc.profile_image_url_https;
        delete doc.profile_image_url;

        db.save(doc._id, doc._rev, doc, (err, res) => {
          if (err) {
            console.error('Unable to update CouchDB');
          }
          // tail call
          process.nextTick(processUpdates);
        });
      }
  });
}

// get a list of all profile_image_url or profile_image_url_https
//   that reference gravatar
db.temporaryView({
  map: function (doc) {
    if ((doc.profile_image_url && doc.profile_image_url.indexOf('gravatar.com') >= 0)
     || (doc.profile_image_url_https && doc.profile_image_https.indexOf('gravatar.com') >= 0)) {
        emit (doc.profile_image_url, doc.profile_image_url_https);
      }
  }
  }, function(err, res) {
    if (err) {
      console.error('CouchDB temporaryView error', err);
      util.exit(1);
    } else {
      res.forEach((key, value, id) => {
        updates.push(id);
      });
    }
    console.log(`${updates.length} mentions to update`);
    processUpdates();
  });

