#!/usr/bin/python

# Copyright 2012 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from datetime import datetime
import couchdb
import hashlib
import json

# retrieve URL including authentication credentials from config JSON
options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['url'])
db = couch[options['db']]

tags = options['tags']
services = ['g+']

"""
# dup by text content
print '============== check by text content ============='
for tag in tags:
    print tag
    for service in services:
        print service
        sums = {}
        for id in db:
            doc = db[id]
            if doc['we_source'] == service and doc['we_tag'] == tag:
                sha1 = hashlib.sha1(doc['text'].encode('utf-8')).hexdigest()
                if sums.has_key(sha1):
                    print 'duplicates:', sums[sha1], id
                    if service == 'twitter':
                        hashtags = doc['entities']['hashtags']
                        print " ",
                        for v in range(len(hashtags)):
                            print hashtags[v]['text'],
                        print
                else:
                    sums[sha1] = id
"""

"""
# dup by timestamp
print '============== check by timestamp ============='
for tag in tags:
    print tag
    #for service in services:
    for service in ['twitter']:
        print service
        stamps = {}
        for id in db:
            doc = db[id]
            if doc['we_source'] == service and doc['we_tag'] == tag:
                stamp = doc['we_timestamp']
                if stamps.has_key(stamp):
                    print 'duplicates:', stamps[stamp], id
                    if service == 'twitter':
                        hashtags = doc['entities']['hashtags']
                        print " ",
                        for v in range(len(hashtags)):
                            print hashtags[v]['text'],
                        print
                else:
                    stamps[stamp] = id
"""

# dup by gplus id
print '============== check by gplus-id ============='

gids = {}

for g in db.view('ids/google', include_docs=True):
    doc = g.doc
    gid = doc['id']
    ourid = g['id']
    # if it hasn't been deleted, remember it
    if not doc.has_key('we_d'):
        if gids.has_key(gid):
            gids[gid].append(ourid)
        else:
            gids[gid] = [ourid]

count = 0
for gid, ids in gids.items():
    count += 1
    if len(ids) > 1:
        print gid, ids
        for id in ids[1:]:
            print "del", id
            doc = db[id]
            doc['we_d'] = True
            doc['we_d_by'] = 'dupbot'
            doc['we_d_at'] = datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%SZ")
            db[id] = doc
print count
