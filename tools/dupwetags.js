var cradle = require('cradle'),
    options = require('../options.json');

var items = {},
    deletes = [],
    updates = {};

var couch = new(cradle.Connection)(options.url),
    mentions = couch.database('mentions');

function doDelete() {
  if (deletes.length <= 0) {
    console.log('done deleting');
    return;
  }

  var thisdup = deletes.pop();
  console.log('doDelete', deletes.length, thisdup._id);
  mentions.remove(thisdup._id, thisdup._rev, function(err) {
    if (err) {
      console.log('*** unable to remove', thisdup._id, thisdup._rev);
      throw new Error('delete error');
    }
    process.nextTick(doDelete);
  });
}

function doUpdates() {
  "use strict";
  var i,
      row,
      updata = [];

  // look through the duplicates
  console.log('==== dups (and multi tags)');
  for (i in items) {
    row = items[i];
    if (row.dups.length > 1) {
      console.log(row.dups.length, row.we_tags,
          row.we_source, row.id, row.text);
    }
  }

  console.log('==== updates');
  for (i in updates) {
    row = items[i];
    if (row.hasOwnProperty('we_tag')) {
      delete row.we_tag
    }
    console.log(row._id, row.we_tags, row.dups.length);
    delete row.dups;
    updata.push(row);
  }

  console.log('updata', updata);  

  mentions.save(updata, function(err) {
    if (err) {
      console.log('*** unable to save updates');
      throw new Error('save error');
    }
    console.log('multitags saved');
    doDelete();
  });
}

mentions.all({include_docs: true},
  function(err, res) {
    "use strict";
    var dups = 0,
        mults = 0;
    if (err) {
      throw new Error('unable to read mentions');
    }
    res.forEach(function (row) {
      if (row._id[0] !== '_') {
        if (!row.id) {
          console.log(row._id,'missing id');
        }
        var item = row.we_source + '|' + row.id;
        if (items.hasOwnProperty(item)) {
          // we've already seen this one,
          //   is it a dup?  or an additional tag?
          if (items[item].we_tags.indexOf(row.we_tag) > -1) {
            // mark new one for deletion
            deletes.push({_id: row._id, _rev: row._rev});
            //console.log(row._id, row.id,
            //    'is duplicate for', items[item]);
            console.log(row._id,'is duplicate for', items[item]._id,
              'from', items[item].we_source);
            console.log(items[item].text);
            items[item].dups.push(row._id);
            console.log('after dup rec', items[item]);
            dups++;
          } else {
            // add the new tag to the old one
            //console.log(row._id, 'has new tag for',
            //    items[item], row.we_tag);
            items[item].we_tags.push(row.we_tag);
            items[item].dups.push(row._id);
            // mark that old one as needing update
            updates[item] = true;
            // mark this new one for deletion
            deletes.push({_id: row._id, _rev: row._rev});
            mults++;
          }
        } else {
          // first time we've seen this source|id
          row.dups = [ row._id ];
          // if we don't already have a we_tags, add one
          if (!row.hasOwnProperty('we_tags')) {
            row.we_tags = [ row.we_tag ];
          }
          items[item] = row;
        }
      }
    });
    console.log('dups', dups, 'mults', mults);
    console.log('deletes', deletes);

    doUpdates();
  });

