#!/usr/bin/python

# use a Google Sheet to set up feeds to harvest
#  sheet-to-feeds.py GOOGLE-SHEETS-URL (readable by bot)

import os
import sys
import json
from hashlib import md5
import urllib2
from bs4 import BeautifulSoup
import couchdb
import gspread

# note: Lida101 has site_id 65 on course.oeru.org

feeds = [
    { from_user': '537jadia', 'from_user_name': 'Jadia7', 'from_user_wp_id': 318, 'site_id': 65', 'from_user_email': 'Kerinesylvester@gmail.com, 'tag': 'lida101', 'feed_url': 'Jadia', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'aakshmita10', 'from_user_name': 'Aakash', 'from_user_wp_id': 161, 'site_id': 65', 'from_user_email': 'akashmita10@gmail.com, 'tag': 'lida101', 'feed_url': 'eaxample.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'aeyz', 'from_user_name': 'Musivale', 'from_user_wp_id': 300, 'site_id': 65', 'from_user_email': 'edwardlusimbo@gmail.com, 'tag': 'lida101', 'feed_url': 'http://CBT.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'amareswaran', 'from_user_name': 'Dr. Naraginti Amareswaran', 'from_user_wp_id': 356, 'site_id': 65', 'from_user_email': 'amareswaran@gmail.com, 'tag': 'lida101', 'feed_url': 'amareswaran', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'angelicamartinez', 'from_user_name': 'Angelica Martinez', 'from_user_wp_id': 223, 'site_id': 65', 'from_user_email': 'angelica.martinez.ochoa@gmail.com, 'tag': 'lida101', 'feed_url': 'https://drive.google.com/open?id=1R5SS7Lma2JFzU4yYT3b0h1QeWHzBNT1E', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'angelntini', 'from_user_name': 'Angelos Konstantinidis', 'from_user_wp_id': 439, 'site_id': 65', 'from_user_email': 'angelntini@gmail.com, 'tag': 'lida101', 'feed_url': 'http://angelos.ict4all.gr/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'anikkumar', 'from_user_name': 'anikkumar', 'from_user_wp_id': 297, 'site_id': 65', 'from_user_email': 'anikkumar551@gmail.com, 'tag': 'lida101', 'feed_url': 'aktv', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'anil', 'from_user_name': 'Anil Prasad P', 'from_user_wp_id': 434, 'site_id': 65', 'from_user_email': 'apletters@gmail.com, 'tag': 'lida101', 'feed_url': 'http://www.wikieducator.org/Anil_Prasad', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'assanakammana', 'from_user_name': 'assanakammana', 'from_user_wp_id': 328, 'site_id': 65', 'from_user_email': 'ak.kayalad@gmail.com, 'tag': 'lida101', 'feed_url': 'lida101', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'atrey', 'from_user_name': 'Ashok Atrey', 'from_user_wp_id': 162, 'site_id': 65', 'from_user_email': 'Ashok.supp@gmail.com, 'tag': 'lida101', 'feed_url': 'Google.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'charlie', 'from_user_name': 'Charlie Gihiala', 'from_user_wp_id': 282, 'site_id': 65', 'from_user_email': 'awocapitalelsuru@gmail.com, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'dave', 'from_user_name': 'dave', 'from_user_wp_id': 2, 'site_id': 65', 'from_user_email': 'dave@oerfoundation.org, 'tag': 'lida101', 'feed_url': 'https://davelane.nz', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'dblwindmill', 'from_user_name': 'James', 'from_user_wp_id': 220, 'site_id': 65', 'from_user_email': 'fantasyworld23@outlook.com, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'dbo1972', 'from_user_name': 'David Bravo Ortiz', 'from_user_wp_id': 252, 'site_id': 65', 'from_user_email': 'davidbravoortiz@yahoo.es, 'tag': 'lida101', 'feed_url': 'trytospeakit.blogspot.com.es', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'deekonda', 'from_user_name': 'deekonda', 'from_user_wp_id': 326, 'site_id': 65', 'from_user_email': 'deekonda2014@gmail.com, 'tag': 'lida101', 'feed_url': 'http://ravi.com/feed.rss', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'devchandr1', 'from_user_name': 'Rama Devchand', 'from_user_wp_id': 431, 'site_id': 65', 'from_user_email': 'devchand_r@usp.ac.fj, 'tag': 'lida101', 'feed_url': 'http://course.oeru.org/lida/101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'dkverma8588', 'from_user_name': 'Durgesh Kumar Verma', 'from_user_wp_id': 294, 'site_id': 65', 'from_user_email': 'durgeshkumarverma4@gmail.com, 'tag': 'lida101', 'feed_url': 'Fisheries science', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'epiotom04', 'from_user_name': 'Epio Tom', 'from_user_wp_id': 372, 'site_id': 65', 'from_user_email': 'epiotom@gmai.com, 'tag': 'lida101', 'feed_url': 'http//www.Google.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'fayrouzelserogy', 'from_user_name': 'Fayrouz Elserogy', 'from_user_wp_id': 128, 'site_id': 65', 'from_user_email': 'fayrouzelserogy@aucegypt.edu, 'tag': 'lida101', 'feed_url': 'fayrouzelserogy', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'hollissankar', 'from_user_name': 'hollissankar', 'from_user_wp_id': 413, 'site_id': 65', 'from_user_email': 'hcsankar@gmail.com, 'tag': 'lida101', 'feed_url': 'https://www.facebook.com/hollis.sankar', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'ismail1', 'from_user_name': 'Ismail', 'from_user_wp_id': 334, 'site_id': 65', 'from_user_email': 'Ismailyassin106@gmail.com, 'tag': 'lida101', 'feed_url': 'Physics', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'ivarawaico1', 'from_user_name': 'Iva Rawaico', 'from_user_wp_id': 429, 'site_id': 65', 'from_user_email': 'ivarawaico@gmail.com, 'tag': 'lida101', 'feed_url': 'ivarawaico@gmail.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'julie440', 'from_user_name': 'Julie Reed', 'from_user_wp_id': 187, 'site_id': 65', 'from_user_email': 'jreed@jcsu.edu, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'justine', 'from_user_name': 'Justine Pepperell', 'from_user_wp_id': 251, 'site_id': 65', 'from_user_email': 'justine.pepperell@gmail.com, 'tag': 'lida101', 'feed_url': 'www.citizenmoderno.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'lifelonglearner4ever', 'from_user_name': 'Ana Rosa', 'from_user_wp_id': 209, 'site_id': 65', 'from_user_email': 'arblue@shaw.ca, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/interactions/course-feed/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'mamafantu', 'from_user_name': 'mamafantu', 'from_user_wp_id': 164, 'site_id': 65', 'from_user_email': 'afratparabodar@gmail.com, 'tag': 'lida101', 'feed_url': 'http://kingtv.org/all-sports/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'mariamagedd', 'from_user_name': 'Maria', 'from_user_wp_id': 134, 'site_id': 65', 'from_user_email': 'mariamaged@aucgept.edu, 'tag': 'lida101', 'feed_url': 'http://mariamaged.wordpress.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'martins', 'from_user_name': 'Ayo', 'from_user_wp_id': 385, 'site_id': 65', 'from_user_email': 'Victorypublishing77@yahoo.com, 'tag': 'lida101', 'feed_url': 'Graphics', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'mingle76', 'from_user_name': 'mingle76', 'from_user_wp_id': 210, 'site_id': 65', 'from_user_email': 'mingle76@gmail.com, 'tag': 'lida101', 'feed_url': 'https://megingle.wordpress.com/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'moasaf', 'from_user_name': 'moasaf', 'from_user_wp_id': 129, 'site_id': 65', 'from_user_email': 'mohamedsafwat@aucegypt.edu, 'tag': 'lida101', 'feed_url': 'https://diglit.creativitycourse.org/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'mohamedhatemm', 'from_user_name': 'Mohamed Hatem', 'from_user_wp_id': 130, 'site_id': 65', 'from_user_email': 'm_hatem@aucegypt.edu, 'tag': 'lida101', 'feed_url': 'https://mohamedhatemblog.wordpress.com/about/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'narayana', 'from_user_name': 'Narayan Prasad Chaudhari', 'from_user_wp_id': 321, 'site_id': 65', 'from_user_email': 'npc.riebhopal@gmail.com, 'tag': 'lida101', 'feed_url': 'https://wikieducator.org/User:Narayana', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'nelliemuller', 'from_user_name': 'NELLIE DEUTSCH', 'from_user_wp_id': 387, 'site_id': 65', 'from_user_email': 'nellie.muller.deutsch@gmail.com, 'tag': 'lida101', 'feed_url': 'https://nellie-deutsch.com/feed.rss', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'nishasingh', 'from_user_name': 'nishasingh', 'from_user_wp_id': 342, 'site_id': 65', 'from_user_email': 'nisha2k04@gmail.com, 'tag': 'lida101', 'feed_url': 'https://myonlinevoyage.blogspot.in/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'niyatipathak02', 'from_user_name': 'napathak02', 'from_user_wp_id': 391, 'site_id': 65', 'from_user_email': 'niyatipathak02@gmail.com, 'tag': 'lida101', 'feed_url': 'niaytipathak.blogspot.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'parveenwrites', 'from_user_name': 'Parveen', 'from_user_wp_id': 172, 'site_id': 65', 'from_user_email': 'parveenwrites@gmail.com, 'tag': 'lida101', 'feed_url': 'Unskilled.in', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'pradipshelke49', 'from_user_name': 'Pradip babasaheb shelke', 'from_user_wp_id': 289, 'site_id': 65', 'from_user_email': 'pradipshelke49@gmail.com, 'tag': 'lida101', 'feed_url': 'www.researchfellowjrf.wordpress.com', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'rajeevtyagi1971', 'from_user_name': 'Dr. Rajeev Tyagi', 'from_user_wp_id': 199, 'site_id': 65', 'from_user_email': 'rajeevtyagi1971@gmail.com, 'tag': 'lida101', 'feed_url': 'drrajeev.in', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'rajeshkrm1545', 'from_user_name': 'Rajesh Kumar', 'from_user_wp_id': 259, 'site_id': 65', 'from_user_email': 'rajeshkrm1545@gmail.com, 'tag': 'lida101', 'feed_url': 'b sc it', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'rekhajoshi', 'from_user_name': 'Rekha', 'from_user_wp_id': 248, 'site_id': 65', 'from_user_email': 'govind.up28@gmail.com, 'tag': 'lida101', 'feed_url': 'DLEd', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'royal2018', 'from_user_name': 'royal2018', 'from_user_wp_id': 358, 'site_id': 65', 'from_user_email': 'theoabel2018@gmail.com, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'sagarmandal18', 'from_user_name': 'Sagar Mandal', 'from_user_wp_id': 304, 'site_id': 65', 'from_user_email': 'sagarmandal.m@gmail.com, 'tag': 'lida101', 'feed_url': 'online learning.edu', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'sasuvosheikh', 'from_user_name': 'SA Shuvo sheikh', 'from_user_wp_id': 330, 'site_id': 65', 'from_user_email': 'mdsuvosheikh@gmail.com, 'tag': 'lida101', 'feed_url': 'facebook.com/diary.unfinished', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'smartsarva', 'from_user_name': 'smartie', 'from_user_wp_id': 264, 'site_id': 65', 'from_user_email': 'srisarvamech@gmail.com, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'ssdd', 'from_user_name': 'Sandeep', 'from_user_wp_id': 240, 'site_id': 65', 'from_user_email': 'sr480029@gmail.com, 'tag': 'lida101', 'feed_url': 'ssdds1087.blogspot.co.in', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'terezinha', 'from_user_name': 'Terezinha Marcondes Diniz Biazi', 'from_user_wp_id': 217, 'site_id': 65', 'from_user_email': 'emebiazi@hotmail.com, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'toddcher', 'from_user_name': 'Cheryl Todd', 'from_user_wp_id': 257, 'site_id': 65', 'from_user_email': 'toddcher@meredith.edu, 'tag': 'lida101', 'feed_url': 'MCOER', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'tommie', 'from_user_name': 'Hamaluba', 'from_user_wp_id': 215, 'site_id': 65', 'from_user_email': 'thamaluba@staff.bou.ac.bw, 'tag': 'lida101', 'feed_url': 'https://www.blogger.com/blogger.g?blogID=7674153765285536169#allposts', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'umavenkata', 'from_user_name': 'Venkata', 'from_user_wp_id': 253, 'site_id': 65', 'from_user_email': 'Umavenkata1992@gmail.com, 'tag': 'lida101', 'feed_url': 'Google', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'uspnauru', 'from_user_name': 'Lisa', 'from_user_wp_id': 206, 'site_id': 65', 'from_user_email': 'mccutchen_l@usp.ac.fj, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'vili', 'from_user_name': 'vili', 'from_user_wp_id': 430, 'site_id': 65', 'from_user_email': 'togavou8@gmail.com, 'tag': 'lida101', 'feed_url': 'none', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'vulaniconfidence', 'from_user_name': 'vulani', 'from_user_wp_id': 355, 'site_id': 65', 'from_user_email': 'prosperitynkalo@gmail.com, 'tag': 'lida101', 'feed_url': 'travel operation', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'wasilatraji007', 'from_user_name': 'wasilat raji', 'from_user_wp_id': 165, 'site_id': 65', 'from_user_email': 'wasilatraji@yahoo.com, 'tag': 'lida101', 'feed_url': 'www.nutrion.comblogfeed URL', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
    { from_user': 'zardavas', 'from_user_name': 'Georgios Zardavas', 'from_user_wp_id': 274, 'site_id': 65', 'from_user_email': 'zardavas@csd.auth.gr, 'tag': 'lida101', 'feed_url': 'https://course.oeru.org/lida101/', 'we_source': 'array-to-feeds.py', 'we_wp_version': 'na', 'type': 'feed' },
]

print feeds

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['localcouch'])
db = couch[options['dbfeeds']]

print sys.argv[1]
#gc = gspread.login(options['gdata']['user'], options['gdata']['pass'])
wks = gc.open_by_url(sys.argv[1]).sheet1

sheet = wks.get_all_values()
print sheet
for row in sheet[STARTROW:]:
    web = row[SHEET_URL]
    page = urllib2.urlopen(web).read()
    soup = BeautifulSoup(page)
    url = ''
    freq = 0
    for l in soup.find_all('link', attrs={
        'rel': 'alternate',
        'type': 'application/rss+xml'}):
        url = l['href']
        freq = 1
        break

    doc = {
            'freq':                 freq,
            'from_user':            row[SHEET_USER],
            'from_user_name':       row[SHEET_FULL_NAME],
            'gravatar':             md5(row[SHEET_EMAIL].lower().strip()).hexdigest(),
            'last_successful':      '2015-01-01T00:00:00.000Z',
            'last_updated':         '2015-01-01T00:00:00.000Z',
            'profile_image_url':    '',
            'profile_image_width':  48,
            'profile_url':          'http://WikiEducator.org/User:' + \
                    row[SHEET_USER].replace(' ', '_'),
            'tags':                 SETTAGS,
            'type':                 'feed',
            'url':                  url,
            'we_tags':              '',
            'web':                  row[SHEET_URL]
            }
    print doc
    db.save(doc)
    print
