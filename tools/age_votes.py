#!/usr/bin/python

## set old=1 on WEnotes votes past a certain age

import os
import json
import time
import couchdb

freshsecs = 2 * 24 * 60 * 60
startkey = '2013-01-01T00:00:00.000Z'
endkey = time.strftime('%Y-%m-%dT%H:%M:%S.000',
       time.gmtime(time.time() - freshsecs))

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['url'])
db = couch[options['dbvotes']]

updates = []

fresh = db.view('vote/WEnotesFresh', include_docs=True)

stale = fresh[startkey:endkey]

for s in stale:
    doc = s.doc
    doc['old'] = 1
    updates.append(doc)

db.update(updates)

