"use strict";

var fs = require('fs'),
    http = require('http'),
    couch = require('couch-client');

var options = JSON.parse(fs.readFileSync('options.json', 'utf8'));
var mentionsdb = couch(options['url'] + '/' + options['db']);

var i;

var indices = [ "a9c59308b71e3784c57332fb3a0445d7" ];
/*  "e4060aa3c4bda527286501c57d071968", "e4060aa3c4bda527286501c57d0721e0",
  "e4060aa3c4bda527286501c57d0722ca", "e4060aa3c4bda527286501c57d072e11",
  "e4060aa3c4bda527286501c57d073d19", "e4060aa3c4bda527286501c57d074497",
  "e4060aa3c4bda527286501c57d074d31", "e4060aa3c4bda527286501c57d075a16"
];*/
/*mentionsdb.view('/mentions/_design/ids/_view/hypothesis', {},
    function(err, doc) {
      console.log('err', err);
      console.log('doc', JSON.stringify(doc));
      for (i = 0; i < doc.total_rows; i++) {
        console.log('removing ' + doc.rows[i].id); // +
        //  ' by ' + doc.rows[i].account.display_name);
        mentionsdb.remove(doc.rows[i].id);
      }
      console.log('all removed, wait for it to happen');
    });*/

for (i = 0; i < indices.length; i++) {
  console.log('index ' + i + ': ' + indices[i]);
  mentionsdb.remove(indices[i]);
}
