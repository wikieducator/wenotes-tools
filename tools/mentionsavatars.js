var cradle = require('cradle'),
    options = require('../options.json');

var avatars = [],
    updates = [];

var couch = new(cradle.Connection)(options.url),
    weavatars = couch.database('weavatars'),
    mentions = couch.database('mentions');

function scanMentions() {
  mentions.view('messages/wikieducator', {include_docs: true},
    function(err, res) {
      if (err) {
        throw new Error('unable to read mentions');
      }
      res.forEach(function (row) {
        if (!row.profile_image_url) {
          if (avatars.hasOwnProperty(row.from_user)) {
            console.log('update ', row._id, 'for', row.from_user);
            row.profile_image_url = avatars[row.from_user];
            updates.push(row);
          }
        }
      });
      console.dir(updates);
      if (updates.length) {
        mentions.save(updates, function (err, res) {
          if (err) {
            throw new Error('unable to update mentions');
          }
          console.log('mentions updated');
        });
      }
    });
}

// cache the avatars we know about
weavatars.all({include_docs: true}, function(err, res) {
  if (err) {
    throw new Error('unable to read weavatars');
  }
  console.dir('weavatars res', res);
  res.forEach(function (row) {
    if (row.url) {
      avatars[row._id] = row.url;
    }
  });
  console.dir(avatars);
  scanMentions();
});

