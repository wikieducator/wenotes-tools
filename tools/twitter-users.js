/*jshint strict:false, node:true */
"use strict";

/*
 * update from_user, from_user_name, and profile_image_url for
 * twitter messages that are missing them (deleting user property)
 *
 * 20141117
 */

var fs = require('fs'),
    couch = require('couch-client');

var options = JSON.parse(fs.readFileSync('options.json', 'utf8'));
var mentionsdb = couch(options.url + '/' + options.db);
var updates = [];

function doUpdates() {
  if (updates.length === 0) {
    return;
  }
  mentionsdb.save(updates.pop(), function(err, doc) {
    if (err) {
      console.log('error saving update', err);
      process.exit();
    }
    process.nextTick(doUpdates);
  });
}

mentionsdb.view('/mentions/_design/ids/_view/twitter', {include_docs: true},
    function(err, doc) {
      var i, thisdoc, count = 0;
      if (err) {
        console.error('err', err);
        process.exit();
      }
      for (i = 0; i < doc.total_rows; i++) {
        if (doc.rows[i].doc.user) {
          thisdoc = doc.rows[i].doc;
          //console.log(i, '========================================================');
          //console.log(thisdoc);
          count = count + 1;
          //console.log(i, '--------------------------------------------------------');
          thisdoc.from_user = thisdoc.user.screen_name;
          thisdoc.from_user_name = thisdoc.user.name;
          thisdoc.profile_image_url = thisdoc.user.profile_image_url;
          delete thisdoc.user;
          delete thisdoc._rev;
          //console.log(thisdoc);
          updates.push(thisdoc);
        }
      }
      console.log('need fixing', count);
      console.log('updates.length', updates.length);
      process.nextTick(doUpdates);
    });

