// @ts-check

/*
 * weavatars
 *
 * - if given command line arg(s), use them as WikiEducator user ids
 *   and force creation of an avatar for each in the database
 *   if there appears to be an image on their user page
 * - with no args look through mentions that are missing a profile image
 * - if --update is given, check user pages for everyone listed in the
 *   weavatars database and if a new image is found, update both
 *   weavatars and all of their posts in mentions
 */
const fs = require('fs');
const axios = require('axios');
const argv = require('minimist')(process.argv.slice(2),
           {boolean: ['update', 'verbose']});
const options = JSON.parse(fs.readFileSync('options.json', 'utf8'));
const nano = require('nano')(options['url']);
const mentionsdb = nano.use(options['db']);
const weavatarsdb = nano.use('weavatars');
const host = 'wikieducator.org';
const port = 443;
const weAPI = `${host}/api.php`;

var arg,
    lookups = 0,
    weusers = new Set(),
    avatars = [],
    fillMissing = false;

function queryString(args, titles) {
  var i;
  var qs = [], t = [];
  for (i=0; i<titles.length; i++) {
    t[i] = encodeURIComponent(titles[i]);
  }
  args.titles = t.join('|');
  for (i in args) {
    qs.push(i + '=' + args[i]);
  }
  return qs.join('&');
}

// return the URL of the thumbnail for the given file,
//   or '' if there is no thumbnail
async function getThumbURL(file) {
  let args = {
    action: 'query',
    format: 'json',
    prop: 'imageinfo',
    iiprop: 'url',
    iiurlwidth: '48',
    iiurlheight: '48'
  }
  let response = await axios.get(`https://${weAPI}?${queryString(args, [file])}`);
  let pages = response.data.query.pages;
  for (let pg in pages) {
    if (pg > 0) {
      return pages[pg].imageinfo[0].thumburl;
    }
  }
  return '';
}

async function updateMentions(c) {
  const body = await mentionsdb.view('messages', 'wikieducator', {
    startkey: c._id,
    endkey: c._id,
    include_docs: true
  });
  for (let mention of body.rows) {
    let doc = mention.doc;
    if (doc.profile_image_url !== c.url) {
      doc.profile_image_url = c.url;
      try {
        await mentionsdb.insert(doc);
      } catch (error) {
        console.error(`inserting ${doc._id}`, error);
      }
    }
  }
}

async function updateAvatars(avatars) {
  console.log('updateAvatars', avatars);
  for (let {_id, file} of avatars) {
    let current = {};
    let newAvatar = false;
    let thumb = await getThumbURL(file);
    try {
      current = await weavatarsdb.get(_id);
      if (current.url !== thumb) {
        console.log(`++++++ ${_id} needs update\n old: ${current.url}\n new: ${thumb}`);
        newAvatar = true;
        current.file = file;
        current.url = thumb;
      }
    } catch (error) {
      console.error(`getting _id ${_id}`, error);
      // assume this is a new avatar document
      newAvatar = true;
      current = {
        _id: _id,
        file: file,
        url: thumb
      };
    }
    if (newAvatar) {
      try {
        await weavatarsdb.insert(current);
      } catch (error) {
        console.error(`inserting ${_id}`, error);
      }
    }
    // if we have a new avatar OR are filling in missing ones
    if (newAvatar || fillMissing) {
      await updateMentions(current);
    }
  }
}

// look for likely profile images in user pages
async function checkUserpages(weusers) {
  let userpages = [];
  for (let user of weusers) {
    userpages.push('User:' + user);
  }

  while (userpages.length > 0) {
    let pages;
    // limit number queried in a single Mediawiki API request
    let todo = (userpages.length > 50) ? 50 : userpages.length;
    let args = {
      action: 'query',
      format: 'json',
      prop: 'revisions',
      rvprop: 'content'
    };
    try {
      let response = await axios.get(`https://${weAPI}?${queryString(args, userpages.splice(0, todo))}`);
      pages = response.data.query.pages;
    } catch (error) {
      console.error(error);
    }

    for (let pg in pages) {
      if (pg > 0) {
        let page = pages[pg]; 
        let content = '';
        if (page.revisions && page.revisions[0]) {
          content = page.revisions[0]['*'];
        }
        var photom = /\|\s*photo\s*=\s*\[\[([^\]|\u200e]+)/i.exec(content);
        var photo = photom ? photom[1] : '';
        var stockm = /(file|image):wikieducator_logo100.jpg/i.exec(photo);
        if (stockm) {
          continue;
        }
        if (photo) {
          avatars.push({
            _id: page.title.substring(5),
            file: photo.replace(/^\s+|\s+$/g, '')});
        }
      }
    }
    userpages = userpages.splice(todo);
  };
  return avatars;
}

async function main() {
  if (argv._.length > 0) {
    for (arg in argv._) {
      weusers.add(argv._[arg]);
      fillMissing = true;
    };
  } else {
    if (argv.update) {
      const body = await weavatarsdb.list();
      body.rows.forEach((doc) => {
        if (! doc.id.startsWith('_')) {
          weusers.add(doc.id);
        }
      });
    } else {
      const body = await mentionsdb.view('messages', 'missing_weavatars');
      body.rows.forEach((doc) => {
        weusers.add(doc['key']);
        fillMissing = true;
      });
    }
  }

  let avatars = await checkUserpages(weusers);
  await updateAvatars(avatars);
}

main();