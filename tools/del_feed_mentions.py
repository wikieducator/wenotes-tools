#!/usr/bin/python

import json
import couchdb
import pprint
pp = pprint.PrettyPrinter(indent=4)

options = json.load(open('../options.json', 'rt'))

couch = couchdb.Server(options['url'])
db = couch[options['db']]

count = 1
mentions = db.view('messages/feed-combined', include_docs=True)
print('number of mentions: %d', len(mentions))
for row in mentions:
    print pp.pformat(row.doc)
    print count #, row
    #print '----', row.id, row.doc.id, row.doc.rev, row.doc
    count += 1
    try:
        if row.doc['we_identifier'] == "blog_feed":
            print "deleting!"
            db.delete(row.doc)
    except AttributeError:
        print "deleting anyway"
        db.delete(row.doc)
