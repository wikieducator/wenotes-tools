/*
 * normalize g+ entries
 *   rename title field to text
 *   add:
 *     from_user and from_user_name = actor.displayName
 *     profile_image_url = actor.image.url
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
/* jshint node:true, esversion:6 */
/* globals emit */
"use strict";

var util = require('util'),
    cradle = require('cradle'),
    options = require('../options.json');

var updates = [];

// split http://user:pass@host:port into a map of its parts
function getConnectionMap(cs) {
  var r = {},
      parts = cs.split('@');
  if (parts.length === 2) {
    let userpass = parts[0].replace(/https?:\/\//, '').split(':');
    if (userpass.length === 2) {
      r.user = userpass[0];
      r.pass = userpass[1];
    }
    parts = parts[1].split(':');
    if (parts.length === 2) {
      r.host = parts[0];
      r.port = parts[1];
    }
  }
  return r;
}

var cdb = getConnectionMap(options.url),
    couch = new(cradle.Connection)(cdb.host, cdb.port, {
      auth: {username: cdb.user, password: cdb.pass}}),
    db = couch.database(options.db);

function processUpdates() {
  var id;
  if (updates.length === 0) {
    return;
  }
  id = updates.pop();
  db.get(id, (err, doc) => {
      if (err) {
        console.log(`Error: unable to fetch ${id}`);
        process.nextTick(processUpdates);
      } else {
        let name = doc.actor.displayName;
        doc.from_user = name;
        doc.from_user_name = name;
        doc.profile_image_url = doc.actor.image.url;
        doc.profile_url = doc.actor.url;
        doc.we_link = doc.url;
        delete doc.url;
        doc.text = doc.title;
        delete doc.title;

        db.save(doc._id, doc._rev, doc, (err, res) => {
          if (err) {
            console.error('Unable to update CouchDB');
          }
          // tail call
          process.nextTick(processUpdates);
        });
      }
  });
}

// get a list of all g+ posts that are missing
// any of the "new normal" fields
db.temporaryView({
  map: function (doc) {
    if ((doc.we_source === 'g+') &&
        (doc.kind !== 'plus#activity') &&
        (!doc.text || !doc.from_user || !doc.from_user_name || !doc.profile_image_url || !doc.we_link || !doc.profile_url)) { 
        emit (doc.actor, null);
      }
  }

  }, function(err, res) {
    if (err) {
      console.error('CouchDB temporaryView error', err);
      util.exit(1);
    } else {
      res.forEach((key, value, id) => {
        updates.push(id);
      });
    }
    console.log(`${updates.length} mentions to update`);
    console.log('ids:', updates);
    processUpdates();
  });

