#!/usr/bin/python

# disable some feeds

import os
import json
import couchdb

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['localcouch'])
db = couch[options['dbfeeds']]

for row in db.view('_all_docs', include_docs=True):
    if row.id[0] == '_':
        continue
    doc = row.doc
    tags = []
    if doc.has_key('tags'):
        tags = doc['tags']
    print row.id, doc['freq'], doc['from_user'], tags
    
    #if doc['freq'] == 0:
    #    # disable people that were registered only for SP4Ed
    #    if len(tags) == 1 and tags[0] == 'ocl4ed':
    #        print "  disable"
    #        doc['freq'] = 0
    #        db[doc.id] = doc

    if doc['freq'] > 0:
        # disable people that were registered only for OCL4Ed
        if len(tags) == 1 and tags[0] == 'ocl4ed':
            print "  disable"
            doc['freq'] = 0
            db[doc.id] = doc

