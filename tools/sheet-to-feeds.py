#!/usr/bin/python

# use a Google Sheet to set up feeds to harvest
#  sheet-to-feeds.py GOOGLE-SHEETS-URL (readable by bot)

import os
import sys
import json
from hashlib import md5
import urllib2
from bs4 import BeautifulSoup
import couchdb
import gspread

STARTROW = 1    # 0 indexed
SETTAGS = ['ds4oer']

SHEET_FULL_NAME = 1     # column indicies
SHEET_EMAIL = 2
SHEET_USER = 3
SHEET_URL = 4

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['localcouch'])
db = couch[options['dbfeeds']]

print sys.argv[1]
gc = gspread.login(options['gdata']['user'], options['gdata']['pass'])
wks = gc.open_by_url(sys.argv[1]).sheet1

sheet = wks.get_all_values()
print sheet
for row in sheet[STARTROW:]:
    web = row[SHEET_URL]
    page = urllib2.urlopen(web).read()
    soup = BeautifulSoup(page)
    url = ''
    freq = 0
    for l in soup.find_all('link', attrs={
        'rel': 'alternate',
        'type': 'application/rss+xml'}):
        url = l['href']
        freq = 1
        break

    doc = {
            'freq':                 freq,
            'from_user':            row[SHEET_USER],
            'from_user_name':       row[SHEET_FULL_NAME],
            'gravatar':             md5(row[SHEET_EMAIL].lower().strip()).hexdigest(),
            'last_successful':      '2015-01-01T00:00:00.000Z',
            'last_updated':         '2015-01-01T00:00:00.000Z',
            'profile_image_url':    '',
            'profile_image_width':  48,
            'profile_url':          'http://WikiEducator.org/User:' + \
                    row[SHEET_USER].replace(' ', '_'),
            'tags':                 SETTAGS,
            'type':                 'feed',
            'url':                  url,
            'we_tags':              '',
            'web':                  row[SHEET_URL]
            }
    print doc
    db.save(doc)
    print

