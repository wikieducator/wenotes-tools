#!/usr/bin/python

""" Pull out WEnotes text for a tag for building wordcloud """

# Copyright 2012 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import couchdb
import json
import re

# retrieve URL including authentication credentials from config JSON
options = json.load(open('options.json', 'rt'))
couch = couchdb.Server(options['localcouch'])
db = couch[options['db']]

f = open('ocl4ed.txt', 'wt')
results = db.view('messages/tag_time', include_docs=True)
for row in results[['ocl4ed', '2012-10-01']:['ocl4ed', '2099-12-31']]:
    source = row.doc['we_source']
    text = row.doc['text']
    if source == 'twitter' or source == 'wikieducator' or source == 'ask':
        user = row.doc['from_user']
        username = row.doc['from_user_name']
    elif source == 'identica':
        user = row.doc['user']['screen_name']
        username = row.doc['user']['name']
    elif source == 'moodle':
        user = row.doc['from_user_name']
        username = user
        if row.doc['truncated']:
            text = text[:-3]
    else:
        print '===== unknown source'
        for k,v in row.doc.items():
            print '    %s: %s' % (k, v)
    # strip out our hashtag
    text = re.sub(r'(?i)#ocl4ed', '', text)
    # strip out retweets
    text = re.sub(r'(?i)rt:?\b', '', text)
    # strip out addressees
    text = re.sub(r'@\S+', '', text)
    # strip out URLs
    text = re.sub(r'(?i)http(s?)://\S+', '', text)
    # strip out elipsis
    text = text.replace('...', ' ')
    text = text.replace('. . .', ' ')
    f.write(text.encode('utf-8'))
    f.write("\n")
f.close()

