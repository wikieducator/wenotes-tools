#!/usr/bin/python

## create an Atom feed of WEnotes for a tag
#
# 20130712 jim@OERfoundation.org
#
# License: MIT

import re
import os
import json
import copy
from datetime import datetime
import couchdb
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.dom.minidom

item_count = 10
dest = '/home/www/oeru/ucan/EDEM630/wenotes.atom'

options = json.load(open(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    os.path.pardir,
    'options.json'), 'rt'))

couch = couchdb.Server(options['url'])
db = couch[options['db']]
tag = 'EDEM630'
tag_specific = True    # make tag-specific feeds?

def prettify(x):
    xm = xml.dom.minidom.parseString(tostring(x))
    return xm.toprettyxml()

def children(parent, elements):
    for (e, t) in elements:
        el = SubElement(parent, e)
        el.text = t

def canonical(d):
    r = copy.deepcopy(d)
    source = d['we_source']
    user = d.get('user', d.get('from_user', ''))
    r['user'] = user
    if source == 'wikieducator':
        r['profileURL'] = 'http://WikiEducator.org/User:' + user
    elif source == 'twitter':
        r['profileURL'] = 'http://twitter.com/' + user
    elif source == 'identica':
        pass
    elif source == 'g+':
        actor = d['actor']
        r['text'] = d['object']['content']
        r['user'] = ''
        r['from_user_name'] = actor['displayName']
        r['profileURL'] = actor['url'].replace('https://', 'http://')
        r['profileIMG'] = actor['image']['url'].replace('https://', 'http://')
        r['we_link'] = d['object']['url']
    elif source == 'feed':
        r['profileURL'] = d['profile_url']
        if r['profileURL'] == '' and d.get('gravatar', '') <> '':
            r['profileURL'] = 'http://gravatar.com/' + d['gravatar']
        r['profileIMG'] = d['profile_image_url']
        if r['profileIMG'] == '' and d.get('gravatar', '') <> '':
            r['profileIMG'] = 'http://gravatar.com/avatar/' + d['gravatar'] \
                    + '?d=identicon'
    elif source == 'ask':
        pass
    elif source == 'moodle':
        pass
    r['text'] = re.sub(r'<[^>]*>', '', r['text'])
    return r

now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

root = Element('feed')
root.set('xmlns', 'http://www.w3.org/2005/Atom')
root.set('xml:lang', 'en')
root.set('xml:base', 'http://OERuniversity.org')
root.set('xmlns:gd', 'http://schemas.google.com/g/2005')

children(root, [
    ('title', 'OERu UCan EDEM630 WEnotes'),
    ('id', 'http://wikieducator.org/Scenario_planning_for_educators/EDEM630'),
    ('updated', now),
    ('generator', 'WEnotes 0.1.0'),
    ])
link = SubElement(root, 'link')
link.set('href', 'http://wikieducator.org/Scenario_planning_for_educators/EDEM630')
linkself = SubElement(root, 'link')
linkself.set('rel', 'self')
linkself.set('href', 'http://UCan.OERuniversity.org/EDEM630/wenotes.atom')

taglc = tag.lower()
mentions = db.view('messages/tag_time', startkey=[taglc, '2099-12-31T00:00:00.000Z'], endkey=[taglc, '2011-01-01T00:00:00.000Z'], descending=True, include_docs=True)
for item in mentions:
    if item.has_key('we_d'):
        continue
    doc = canonical(item.doc)
    entry = SubElement(root, 'entry')
    source = doc['we_source']
    if source == 'feed':
        source = 'blog'
    title = "%s (%s)" % (doc['from_user_name'], source)
    children(entry, [
        ('id', doc['we_link']),
        ('title', title),
        ('updated', doc['we_timestamp']),
        ('summary', doc['text']),
        ('content', doc['text'])
        ])
    link = SubElement(entry, 'link')
    link.set('href', doc['we_link'])
    linkalt = SubElement(entry, 'link')
    linkalt.set('rel', 'alternate')
    linkalt.set('type', 'text/html')
    linkalt.set('href', doc['we_link'])

    author = SubElement(entry, 'author')
    children(author, [
        ('name', doc['from_user_name']),
        ('uri', doc['profileURL']),
        ('email', 'noreply@OERuniversity.org'),
        ])
    image = SubElement(author, 'gd:image')
    image.set('rel', 'http://schemas.google.com/g/2005#thumbnail')
    image.set('src', doc['profileIMG'])

    category = SubElement(entry, 'category')
    category.set('term', 'EDEM630')

    item_count -= 1
    if item_count <= 0:
        break

#print prettify(root)
open(dest, 'wt').write('<?xml version="1.0" encoding="utf-8"?>\n'
        + tostring(root))

