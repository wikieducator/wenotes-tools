#!/usr/bin/python

## create an Atom feed of WEnotes for a tag
#
# 20130712 jim@OERfoundation.org
#
# License: MIT

import re
import os
import json
import copy
from datetime import datetime
import couchdb
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.dom.minidom
from bottle import route, run, default_app, response

item_count = 30
hostname = 'wenotes.wikieducator.org'

options = json.load(open('./options.json', 'rt'))
couchurl = options['localcouch']
dbname = options['db']

couch = couchdb.Server(couchurl)
db = couch[dbname]

def prettify(x):
    xm = xml.dom.minidom.parseString(tostring(x))
    return xm.toprettyxml()

def children(parent, elements):
    for (e, t) in elements:
        el = SubElement(parent, e)
        el.text = t

def unHTML(s):
    s = re.sub(r'<[^>]*>', '', s)
    s = s.replace('&amp;', '&')
    s = s.replace('&#39;', "'")
    s = s.replace('&quot;', '"')
    return s

def canonical(d):
    r = copy.deepcopy(d)

    #print r
    source = d['we_source']
    user = d.get('user', d.get('from_user', ''))
    r['user'] = user
    if source == 'wikieducator':
        r['profileURL'] = 'http://WikiEducator.org/User:' + user
        r['profileURL'] = r['profileURL'].replace(' ', '_')
        r['profileIMG'] = r['profile_image_url']
        r['we_link'] = 'http://wikieducator.org/Special:WEnotes?wenoteid=%s' % (r['_id'])
    elif source == 'twitter':
        r['profileURL'] = 'http://twitter.com/' + user
        r['profileIMG'] = r['profile_image_url']
        r['we_link'] = r['profileURL'] + '/status/' + r['id_str']
    elif source == 'identica':
        pass
    elif source == 'g+' or source == 'gplus':
        #for k,v in r.items():
        #    print k,'=',v
        actor = d['actor']
        r['text'] = d['object']['content']
        r['user'] = ''
        r['from_user_name'] = actor['displayName']
        r['profileURL'] = actor['url'].replace('https://', 'http://')
        r['profileIMG'] = actor['image']['url'].replace(
                'https://', 'http://')
        r['we_link'] = d['url']
        r['title'] = unHTML(r['title'])
    elif source == 'feed':
        r['profileURL'] = d['profile_url']
        r['profileURL'] = r['profileURL'].replace(' ', '_')
        if r['profileURL'] == '' and d.get('gravatar', '') <> '':
            r['profileURL'] = 'http://gravatar.com/' + d['gravatar']
        r['profileIMG'] = d['profile_image_url']
        if r['profileIMG'] == '' and d.get('gravatar', '') <> '':
            r['profileIMG'] = 'http://gravatar.com/avatar/' + \
                    d['gravatar'] + '?d=identicon'
    elif source == 'ask':
        pass
    elif source == 'moodle':
        pass
    r['text'] = unHTML(r['text'])

    # if we still don't have a profileIMG, but have a gravatar hash use it
    if r['profileIMG'] == '' and r.has_key('gravatar') \
        and r['gravatar'] <> '':
            r['profileIMG'] = 'http://www.gravatar.com/avatar/%s?s=48&d=identicon' % r['gravatar']
    return r

def return_feed(tag, kinds, item_count=20):
    kinds = kinds.split(',')
    # latest update
    latest = '2010-01-01T00:00:00Z'
    now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

    root = Element('feed')
    root.set('xmlns', 'http://www.w3.org/2005/Atom')
    root.set('xml:lang', 'en')
    root.set('xml:base', 'http://OERfoundation.org')
    root.set('xmlns:gd', 'http://schemas.google.com/g/2005')

    children(root, [
        ('title', 'WEnotes %s' % tag),
        ('id', 'http://wenotes.wikieducator.org/atom/%s' % tag),
        ('generator', 'WEnotes 0.1.0'),
        ('logo', 'http://t.oerfoundation.org/images/OERu_logo_1420x710.png'),
        ('icon', 'http://t.oerfoundation.org/images/OERu_logo.ico')
        ])
    # updated.text will be filled in after we've scanned through entries
    updated = SubElement(root, 'updated')
    link = SubElement(root, 'link')
    link.set('href', 'http://wikieducator.org/%s' % tag)
    linkself = SubElement(root, 'link')
    linkself.set('rel', 'self')
    if '*' in kinds:
        linkself.set('href', 'http://%s/atom/%s' % (hostname,tag))
    else:
        linkself.set('href', 'http://%s/atom/%s/%s' %
               (hostname, tag, ",".join(kinds)))

    if 'blog' in kinds:
        kinds.append('feed')
    if 'gplus' in kinds:
        kinds.append('g+')
    taglc = tag.lower()
    mentions = db.view('messages/tag_time', startkey=[taglc, '2099-12-31T00:00:00.000Z'], endkey=[taglc, '2011-01-01T00:00:00.000Z'], descending=True, include_docs=True)
    for item in mentions:
        if item.has_key('we_d'):
            continue
        if (not '*' in kinds) and (not item.doc['we_source'] in kinds):
            continue
        doc = canonical(item.doc)
        entry = SubElement(root, 'entry')
        source = doc['we_source']
        if source == 'feed':
            source = 'blog'
        if doc.has_key('title'):
            tstr = doc['title']
        else:
            words = doc['text'].split(' ')
            words = filter(lambda w: len(w)>0 and w[0]<>'@' and w<>'RT', words)
            tstr = " ".join(words[:5])
        # don't allow a zero length title
        if len(tstr) == 0:
            tstr = doc['from_user_name']
        if kinds == '*':
            tstr += ' (%s)' % source
        title = tstr
        children(entry, [
            ('id', doc['we_link']),
            ('title', title),
            ('updated', doc['we_timestamp']),
            ('summary', doc['text']),
            ('content', doc['text'])
            ])
        if doc['we_timestamp'] > latest:
            latest = doc['we_timestamp']
        link = SubElement(entry, 'link')
        link.set('href', doc['we_link'])
        linkalt = SubElement(entry, 'link')
        linkalt.set('rel', 'alternate')
        linkalt.set('type', 'text/html')
        linkalt.set('href', doc['we_link'])

        author = SubElement(entry, 'author')
        children(author, [
            ('name', doc['from_user_name']),
            ('uri', doc['profileURL']),
            ('email', 'noreply@OERfoundation.org'),
            ])
        image = SubElement(author, 'gd:image')
        image.set('rel', 'http://schemas.google.com/g/2005#thumbnail')
        image.set('src', doc['profileIMG'])

        category = SubElement(entry, 'category')
        category.set('term', tag)

        item_count -= 1
        if item_count <= 0:
            break

    updated.text = latest

    #print prettify(root)
    return prettify(root)
    return '<?xml version="1.0" encoding="utf-8"?>\n' + tostring(root)

@route('/atom/<tag>')
def feed(tag='wenotes'):
    global item_count
    #for k,v in os.environ.items():
    #    print k,"=",v
    #print "tag %s" % tag
    print tag
    response.content_type = 'application/atom+xml; charset=utf-8'
    return return_feed(tag, '*', item_count)

@route('/atom/<tag>/<kinds>')
def typefeed(tag='wenotes', kinds='*'):
    global item_count
    print tag, kinds
    response.content_type = 'application/atom+xml; charset=utf-8'
    return return_feed(tag, kinds, item_count)

if __name__ == "__main__":
    # Interactive mode
    run(host='10.10.10.2', port=3000)
else:
    # Mod WSGI launch
    #os.chdir(os.path.dirname(__file__))
    application = default_app()
