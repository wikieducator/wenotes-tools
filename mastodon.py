#!/usr/bin/python

# Copyright 2017 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime
import cookielib
import urllib, urllib2
import couchdb
# to deal with nasty characters included in Hypothesis quotes
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# others
import json
# for debugging
import logging
import pprint

# Defaults
DEBUG = False

# return a tuple (text, truncated) of abridged text and truncated flag
def abridge(text):
    truncated = False
    # strip out HTML comments (and MSOffice conditionals)
    abridged = re.sub(r'<!--.*?-->', '', text)
    # strip out HTML markup before abridging,
    #   so we don't stop midtag
    abridged = re.sub(r'<[^>]*>', '', abridged)
    abridged = re.sub(r'\s*by [^.]+\.\n?', '', abridged)
    abridged = abridged[:500].strip()
    abridged = abridged.replace('&nbsp;', ' ')
    abridged = abridged.replace('&#8211;', "--")
    abridged = abridged.replace('&#8216;', "'")
    abridged = abridged.replace('&#8217;', "'")
    abridged = abridged.replace('&#8220;', '"')
    abridged = abridged.replace('&#8221;', '"')
    abridged = abridged.replace('&#8230;', "...")
    abridged = abridged.replace('&#38;', "&")
    abridged = abridged.replace('\n', ' ')
    # get rid of multiple spaces (which the above may have introduced)
    abridged = re.sub(r'  +', ' ', abridged)
    i = len(abridged)
    if i > 200:
        i = 200
        while abridged[i] != ' ' and i > 0:
            i -= 1
        abridged = abridged[:i] + '...'
        truncated = True
    return (abridged, truncated)

def formatToot(toot, tag):
    timestamp = toot['created_at']
    dt = datetime.strptime(toot['created_at'][0:18], '%Y-%m-%dT%H:%M:%S')
    toot['we_timestamp'] = dt.isoformat() + '.000Z'
    logging.debug('timestamp: %s', toot['we_timestamp'])
    toot['we_source'] = 'mastodon'
    toot['we_tags'] = [tag]
    # convert native structure to compatible structure for WEnotes.js
    logging.debug('unabridged content: %s' % toot['content'])
    (abridged, truncated) = abridge(toot['content']) # strip HTML tags
    logging.debug('abridged content: %s' % abridged)
    toot['text'] = abridged
    toot['truncated'] = truncated
    #logging.debug('user URL: %s', toot['account']['url'])
    toot['profile_url'] = toot['account']['url']
    logging.debug('toot text: %s', pp.pformat(toot['text']))
    toot['user'] = {}
    toot['user']['screen_name'] = toot['account']['username']
    toot['user']['name'] = toot['account']['display_name']
    toot['user']['profile_image_url'] = toot['account']['avatar_static']
    return toot

def getToots(newToots, tag, sinceID):
    url = '%s/%s?since_id=%d' % (options['mastodon']['rss_url'], tag, sinceID)
    response = urllib2.urlopen(url)
    toots = json.loads(response.read())
    logging.debug("processing %s.", tag)
    for toot in toots:
        logging.debug('checking %d against max %d', int(toot['id']), int(sinceID))
        if int(toot['id']) > int(sinceID):
            id = toot['id']
            if newToots.has_key(id):
                logging.info("appending tag %s", tag)
                newToots[id]['we_tags'].append(tag)
            else:
                logging.info("new interesting toot %s (%s)", id, tag)
                newToots[id] = formatToot(toot, tag)
    return

# retrieve URL including authentication cretootials from config JSON
options = json.load(open('./options.json', 'rt'))

#logging configuration
LogLevel = logging.DEBUG # or logging.INFO or logging.WARN, etc.
#LogLevel = logging.INFO # or logging.INFO or logging.WARN, etc.
LogFilename = options['logdir'] + '/mastodon.log'
LogFormat = '%(asctime)s - %(levelname)s: %(message)s'
print 'logfile %s, level %s' % (LogFilename, LogLevel)
logging.basicConfig(format=LogFormat,level=LogLevel,filename=LogFilename)
# Set up the prettyprinter object for debugging
pp = pprint.PrettyPrinter(indent=4)

# couch database
couch = couchdb.Server(options['url'])
#couch = couchdb.Server('https://bot:QX8hsyZmvvT7eS@couch.oerfoundation.org')
db = couch[options['db']]

# get tag list from URL
tagurl = options['settings-url']
jsoncontent = urllib.urlopen(tagurl)
reference_tags = json.loads(jsoncontent.read())["tags"]
# set header this app will report itself as
headers = {'User-Agent' : 'WEnotes-Mastodon/0.3'}

#for row in db.view('ids/mastodon_stats', group=True):
last = {}
for row in db.view('ids/mastodon_max'):
    #logging.debug("key = %s, value = %s", row.key, row.value)
    try:
        if int(row.value) > last[str(row.key)]:
            logging.debug('new max for %s = %d', row.key, int(row.value))
            last[str(row.key)] = int(row.value);
    except:
        last[str(row.key)] = int(row.value);
logging.debug('maxes: %s', pp.pformat(last))

# build a dictionary, indexed by ID so we can quickly merge we_tags
newToots = {}
for tag in reference_tags:
    try:
        logging.debug("last[%s] = %d", tag, last.get(str(tag), 0))
        getToots(newToots, tag, last.get(str(tag), 0))
    except urllib2.HTTPError:
        logging.warning("mastodon: unable to fetch tag %s", tag)
        pass

# convert to a list
newToots = newToots.values()
if len(newToots):
    if DEBUG:
        logging.debug('%s new toots, %s', len(newToots), pp.pformat(newToots))
    for doc in db.update(newToots):
        logging.info('adding toot: %s', repr(doc))
