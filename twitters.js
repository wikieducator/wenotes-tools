/*
 * watch a Twitter stream of option.tags
 *
 * Copyright 2012-2016 Open Education Resource Foundation
 * @license MIT
 */
"use strict";

var fs = require('fs'),
    cradle = require('cradle'),
    Log = require('log'),
    Twitter = require('node-tweet-stream'),
    options = require('./options.json');

var i, d;

var db = new(cradle.Connection)(options.couch.url, options.couch.port,
      {auth: {username: options.couch.user, password: options.couch.pass}}).database(options.db);

var tags = options.tags.map(function(s) { return '#' + s; });

var log = new Log(options.log.twitters.level,
              fs.createWriteStream(options.log.twitters.file, {
                flags: 'a'}));

log.info('tags:', tags.join());

// if options use access_token_(key|secret),
//   convert to token and token_secret for node-tweet-stream
if (options.twitter.access_token_key) {
  options.twitter.token = options.twitter.access_token_key;
  delete options.twitter.access_token_key;
  options.twitter.token_secret = options.twitter.access_token_secret;
  delete options.twitter.access_token_secret;
}

var tweet = new Twitter(options.twitter);

tweet.on('tweet', function(data) {
  var tag;
  console.log('tweet');
  log.debug('=====');
  log.debug(data);
  // copy out the interesting properties inline
  d = {};
  d.created_at = data.created_at;
  //d.entities = JSON.decode(JSON.encode(data.entities));
  d.entities = {};
  d.entities.hashtags = [];
  d.entities.urls = [];
  d.we_tags = [];
  for (i=0; i<data.entities.hashtags.length; i++) {
    tag = data.entities.hashtags[i].text.toLowerCase();
    log.debug('***', tag);
    // copy the tags we look for, and that are unique
    if ((options.tags.indexOf(tag) >= 0) && (d.we_tags.indexOf(tag) === -1)) {
      d.we_tags.push(tag);
    } else if (options.blacklist.indexOf(tag) >= 0) {
      // don't bother saving tweets containing blacklisted tags
      // (or alternatively, save with we_d=true)
      return;
    }
    d.entities.hashtags.push(data.entities.hashtags[i]);
  }
  for (i=0; i<data.entities.urls.length; i++) {
    d.entities.urls.push(data.entities.urls[i]);
  }
  d.from_user = data.user.screen_name;
  d.from_user_name = data.user.name;
  d.from_user_id = data.user.id;
  d.from_user_id_str = data.user.id_str;
  d.id = data.id;
  d.id_str = data.id_str;
  d.profile_image_url = data.user.profile_image_url;
  d.profile_image_url_https = data.user.profile_image_url_https;
  d.followers_count = data.user.followers_count;
  d.statuses_count = data.user.statuses_count;
  d.source = data.source;
  d.retweeted = data.retweeted;
  d.text = data.text;
  d.to_user_id = data.in_reply_to_user_id;
  d.to_user_id_str = data.in_reply_to_user_id_str;
  d.in_reply_to_status_id = data.in_reply_to_status_id;
  d.in_reply_to_status_id_str = data.in_reply_to_status_id_str;
  d.we_source = 'twitter';
  var timestamp = new Date(data.created_at);
  d.we_timestamp = JSON.stringify(timestamp).replace(/"/g, '');
  log.debug('----------------');
  log.debug(d);
  db.save(d, function (err, res) {
    if (err) {
      log.error('***failed to save: ' + d.id_str + ' ' +
          d.we_timestamp);
    } else {
      log.info('---saved: ' + d.id_str, res);
    }
  });
});

tweet.on('error', function(error) {
  console.error('***error', error);
  throw error;
});

tweet.on('delete', function(d) {
  console.log('   delete');
  log.debug('delete');
  log.debug(d);
});

tweet.on('scrub_geo', function(d) {
  console.log('   scrub_geo', d);
});

tweet.on('limit', function(d) {
  console.log('   limit', d);
});

tweet.on('status_withheld', function(d) {
  console.log('   status withheld', d);
});

tweet.on('disconnect', function(d) {
  console.error('***disconnected', d);
});

tweet.on('warning', function(d) {
  console.error('***warning', d);
});

tweet.track(tags);
