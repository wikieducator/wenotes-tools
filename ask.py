#!/usr/bin/python

# Copyright 2012 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime
import couchdb
import feedparser
import sys
import json
import hashlib
import HTMLParser

# retrieve URL including authentication credentials from config JSON
options = json.load(open('./options.json', 'rt'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]
h = HTMLParser.HTMLParser()
tags = options['tags']

# serial number of messages retrieved this poll to uniquify ID
serial = 0

# get the last time for a ask post in the database
view = db.view('ids/ask', descending=True, limit=1)
if len(view) == 1:
    for row in view:
        lasttime = row.key
else:
    lasttime = "2000-01-01T00:00:00.000Z"

def gravatar(e):
    return 'http://www.gravatar.com/avatar/' + \
            hashlib.md5(e.strip().lower()).hexdigest() + '?s=48&d=identicon&r=pg'

def process_tag(tag):
    def comment_or_answer(mo):
        if mo.group(1) == 'Comment':
            return 'Re:'
        return 'Answer:'

    global serial
    feed = 'http://ask.oeruniversity.org/feeds/atom/?tags=' + tag
    qfeed = 'http://ask.oeruniversity.org/feeds/questiona/%s/'
    qpattern = re.compile(r'http://ask.OERuniversity.org/question/(?P<q>\d+)')

    # find all of the questions
    rss = feedparser.parse(feed)
    qitems = rss['items']
    qitems.reverse()

    qs = []
    for qitem in qitems:
        mo = qpattern.match(qitem['link'])
        if mo:
            qs.append(mo.group('q'))

    # for each of the questions, find the new questions, answers, comments
    for q in qs:
        rss = feedparser.parse(qfeed % q)
        feedtitle = rss['channel']['title']

        items = rss['items']

        for item in items:
            if item['title'] == 'RSS Error' and item['description'] == 'Error reading RSS data':
                break
            truncated = False
            dt = datetime.strptime(item['date'], '%Y-%m-%dT%H:%M:%S+00:00')
            we_timestamp = dt.strftime('%Y-%m-%dT%H:%M:%S.000Z')
            if we_timestamp <= lasttime:
                continue
            seconds = time.mktime(dt.timetuple())
            # strip out HTML markup before abridging, so we don't stop midtag
            body = item['title'] + ' ' + item['summary']
            abridged = re.sub(r'<[^>]*>', '', body)
            abridged = h.unescape(abridged)
            # remove square brackets (link anchors)
            abridged = re.sub(r'\[|]', ' ', abridged)
            abridged = re.sub(r'\s+', ' ', abridged)
            # remove inline attribution, already have author
            abridged = re.sub(r'(Comment|Answer) by (.*?) for',
                    comment_or_answer, abridged, 1)
            abridged = abridged[:500].strip()
            abridged = abridged.replace('&nbsp;', ' ')
            abridged = abridged.replace('\n', ' ')
            i = len(abridged)
            if i > 137:
                i = 137
                while abridged[i] != ' ' and i > 0:
                    i -= 1
                abridged = abridged[:i] + '...'
                truncated = True

            author = item['author_detail']['name']

            mention = {
                    'from_user': author,
                    'from_user_name': author,
                    'created_at': item['date'],
                    'profile_image_url':
                        gravatar(item['author_detail']['email']),
                    'text': abridged,
                    'truncated': truncated,
                    'id': '%d%05d%03d' % (seconds, int(q), serial),
                    'profile_url': item['author_detail']['href'],
                    'we_source': 'ask',
                    'we_feed': '%s: %d' % (feedtitle, int(q)),
                    'we_tags': [tag],
                    'we_timestamp': we_timestamp,
                    'we_link': item['link']
                    }
            if tag == 'sp4edu':
                mention['we_tags'] = ['sp4ed']
            print mention
            print '==========='
            db.save(mention)
            serial += 1

for tag in tags:
    process_tag(tag)
