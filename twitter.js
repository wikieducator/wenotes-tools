/**
    Twitter polling harvester
    @author Jim Tittsler <jim@OERfoundation.org>
    @license MIT
 **/

var http = require('http'),
    fs = require('fs'),
    Log = require('log'),
    couch = require('couch-client'),
    options = require('./options.json');

// method level, default is 'debug':0, 'info':1, 'notice':2, 'warning':3, 'error':4, 'critical':5,
// 'alert':6 and 'emergency':7
var log = new Log(options.log.twitter.level,
  fs.createWriteStream(options.log.twitter.file, {
  flags: 'a'})
);

var mentionsdb = couch(options.url + '/' + options.db);
var tags = options.tags;
var tagslen = tags.length;
var i;
for (i=0; i<tagslen; i++) {
  tag = tags[i];
  log.debug('tag = ' + tag)
  tags[i] = '#' + tag;
}

log.info('tags:', tags.join());


function getTweets(sinceID) {
  "use strict";
  var i, query, body,
    regexps = [],
    options = {};
  for (i=0; i<tags.length; i++) {
    regexps[i] = new RegExp(tags[i], "i");
  }
  query = encodeURIComponent(tags.join(' OR '));

  options = {
    host: 'api.twitter.com',
    port: 443,
    path: '/1.1/search/tweets.json?q=' + query + '&include_entities=true&since_id=' + sinceID
  };

  http.get(options, function(res) {
    var r, we_tags;
    body = '';
    var status = res.statusCode;
    var headers = JSON.stringify(res.headers);
    res.on('data', function(chunk) {
      body += chunk;
    });
    res.on('end', function() {
      try {
        r = JSON.parse(body);
      } catch(err) {
        console.log('*** Unable to parse body', err);
        console.log(status);
        console.log(headers);
        console.log(body);
        log.error('*** Unable to parse body', err);
        log.info(status);
        log.debug(headers);
        log.debug(body);
        throw(err);
      }
      if (r.results) {
        var l = r.results.length - 1;
        while (l >= 0) {
          var d = new Date(r.results[l].created_at);
          r.results[l].we_timestamp = JSON.stringify(d).replace(/"/g, '');
          r.results[l].we_source = 'twitter';
          we_tags = [];
          for (i=0; i<tags.length; i++) {
            if(regexps[i].test(r.results[l].text)) {
              we_tags.push(tags[i].substring(1));
            }
          }
          r.results[l].we_tags = we_tags;
          // get a deep copy of r.results[l]
          mentionsdb.save(JSON.parse(JSON.stringify(r.results[l])));
          l -= 1;
        }
      }
    });
  });
}

// get the last id
mentionsdb.view('/mentions/_design/ids/_view/twitter',
  {descending: true, limit: 1}, function(err, doc) {
    "use strict";
    if (err) throw err;
    getTweets((doc.rows.length > 0) ? doc.rows[0].value : 0);
  }
);
