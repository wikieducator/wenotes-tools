#!/usr/bin/python

""" Harvest bookmarks with tags specified in options file."""

# Copyright 2017 Open Education Resource Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import time
from datetime import datetime, timedelta
import couchdb
import urllib
import json
import feedparser
import argparse
import requests
import HTMLParser
import hashlib
# for debugging
import logging
import pprint
# to fix various unicode issues
import sys
reload(sys)
sys.setdefaultencoding('utf8')

# version info
scanner = 'WEnotes Semantic Scuttle (Bookmarks) Scanner'
scanner_version = '0.2'

DEBUG = False
#DEBUG = True
DELAY = 1.0         # delay in seconds between Discourse hits
MAX_TEXT_LEN = 300  # max characters before truncation

# retrieve URL from config JSON
options = json.load(open('./options.json', 'r'))
couch = couchdb.Server(options['url'])
db = couch[options['db']]
# get tag list from URL
tagurl = options['settings-url']
jsoncontent = urllib.urlopen(tagurl)
reference_tags = json.loads(jsoncontent.read())["tags"]
# set header this app will report itself as
headers = {'User-Agent' : 'WEnotes-Bookmarks/0.1'}
# the URL of the bookmarks RSS feed
bookmarks_url = options["bookmarks"]["url"]
bookmarks_rss_url = bookmarks_url + '/rss.php/all'
# for parsing HTML in bookmark text
h = HTMLParser.HTMLParser()

# length of content for messages
message_length = 200;

#logging configuration
LogLevel = logging.DEBUG # or logging.INFO or logging.WARN, etc.
#LogLevel = logging.INFO # or logging.INFO or logging.WARN, etc.
LogFilename = options['logdir'] + '/bookmarks.log'
LogFormat = '%(asctime)s - %(levelname)s: %(message)s'
print 'logfile %s, level %s' % (LogFilename, LogLevel)
logging.basicConfig(format=LogFormat,level=LogLevel,filename=LogFilename)

# initialising
lasttime = "2000-01-01T00:00:00.000Z"

# setting up the parser for RSS
parser = argparse.ArgumentParser(description='Harvest posts from Bookmarks, our Semantic Scuttle instance.')
parser.add_argument('-f', '--full', action='store_false',
        help='get list of categories, and then every topic in each')
args = parser.parse_args([])

# Set up the prettyprinter object for debugging
pp = pprint.PrettyPrinter(indent=4)

# get all the mention ids currently in the db so we can
# ensure we don't duplicate
all_mentions = db.view('ids/bookmarks')

logging.debug('avoiding these mentions we already have: %s', pp.pformat(all_mentions))

# check if we have this mention already
def have_mention(msg_id):
    """Return boolean showing if we already have this message."""
    #print 'id = %s' % msg_id
    for mention in all_mentions:
        if msg_id==mention['value']:
            #print 'Found id %s' % msg_id
            return True
    logging.debug('failed to find %s', msg_id)
    return False

# deal with the +0000 time offset, not supported by datetime
# see https://stackoverflow.com/questions/23940551/why-z-is-not-supported-by-pythons-strptime
def dt_parse(t):
    ret = datetime.strptime(t[0:24], '%a, %d %b %Y %H:%M:%S')
    if t[26]=='+':
        ret += timedelta(hours=int(t[27:30]))
    elif t[26]=='-':
        ret -= timedelta(hours=int(t[27:30]))
    return ret

# find all of bookmarks
rss = feedparser.parse(bookmarks_rss_url)
# find the channel title
feedtitle = rss['channel']['title']

items = rss['items']
# reverse them, so oldest is first
items.reverse()
logging.debug("found %d items", len(items))

# for each item in RSS check if it has one (or more) of our tags
for item in items:
    # is this an error item? If so, bail
    if item['title'] == 'RSS Error' and item['description'] == 'Error reading RSS data':
        break
    logging.debug("looking at bookmark: %s", item['title'])
    # is this an item with a relevant tag...
    try:
       taglist = [t['term'] for t in item['tags']]
    except:
       logging.debug("no tags defined for %s", item['title'])
       continue
    common_tags = list(set(taglist) & set(reference_tags))
    logging.debug("taglist: %s\nreference tags: %s\ncommon tags: %s", taglist, reference_tags, common_tags)
    #logging.debug("common tags: %s", common_tags)
    if not common_tags:
        continue
    # initialise
    truncated = False
    dt = dt_parse(item['published'])
    we_timestamp = dt.strftime('%Y-%m-%dT%H:%M:%S.000Z')
    if we_timestamp <= lasttime:
        continue
    seconds = time.mktime(dt.timetuple())
    # check if we've seen the gid before...
    if have_mention(item['id']):
        continue
    # strip out HTML markup before abridging, so we don't stop midtag
    body = item['title'] + ' ' + item['summary']
    # pull out all the html tags
    abridged = re.sub(r'<[^>]*>', '', body)
    # remove any escaped tags
    abridged = h.unescape(abridged)
    # remove square brackets (link anchors)
    abridged = re.sub(r'\[|]', ' ', abridged)
    # remove multiple spaces
    abridged = re.sub(r'\s+', ' ', abridged)
    # remove line feeds and non-breaking spaces
    abridged = abridged.replace('&nbsp;', ' ')
    abridged = abridged.replace('\n', ' ')
    # abridge to message_length characters + ... (i.e. 3)
    # + 'Link added - ' (i.e. 13)
    # + url of max 32 char, or 48
    i = len(abridged)
    if i > (message_length - 48):
        i = (message_length - 48)
        while abridged[i] != ' ' and i > 0:
            i -= 1
        abridged = abridged[:i] + '...'
        truncated = True
    # prepend link:
    abridged = 'Link added: ' + item['link'] + ' - ' + abridged
    # get author's name
    author = item['author_detail']['name']
    # username
    username = item['author']
    # create the mention object
    mention = {
        'user': {
            'name': item['contributors'][0]['name'],
            'username': item['contributors'][0]['href'].rsplit('/', 1)[-1],
            'profile_url': item['contributors'][0]['href']
        },
        'from_user_name': author,
        'created_at': item['published'],
        'text': abridged,
        'truncated': truncated,
        'id': item['id'],
        #'profile_url': item['author'],
        'we_source': 'bookmarks',
        'we_feed': '%s' % (feedtitle),
        'we_tags': common_tags,
        'we_timestamp': we_timestamp,
        'we_scanner': scanner,
        'we_scanner_version': scanner_version,
        'we_link': item['link']
    }
    #pp.pprint(mention)
    logging.info('adding %s', item['id'])
    if DEBUG:
      logging.debug('or would be if debugging was off')
    else:
      db.save(mention)
